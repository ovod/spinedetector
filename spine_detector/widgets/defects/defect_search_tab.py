from PyQt5 import QtWidgets

from spine_detector import event_logic
from spine_detector.constants import LOGGER
from spine_detector.widgets.defects.common_defect_widget import DefectSearcherWidget


class DefectSearcherTab(QtWidgets.QWidget, event_logic.Component):
    def __init__(self, parent=None):
        super(DefectSearcherTab, self).__init__(parent=parent)

        self.setFixedSize(1200, 800)

        central_layout = QtWidgets.QVBoxLayout(self)

        self.stage_enabled_rbtn = QtWidgets.QRadioButton(self)
        self.stage_enabled_rbtn.setText('Defect searching')
        self.stage_enabled_rbtn.setAutoExclusive(False)
        self.stage_enabled_rbtn.setEnabled(False)
        central_layout.addWidget(self.stage_enabled_rbtn)

        self.child_widget = DefectSearcherWidget(self)
        central_layout.addWidget(self.child_widget)
        self.post_init()

    def post_init(self):
        self.stage_name = 'defect_search'

        self.stage_enabled = False
        self.stage_enabled_rbtn.setEnabled(False)
        self.stage_enabled_rbtn.clicked.connect(self.stage_enabled_rbtn_click)
        self.child_widget.setEnabled(False)

        self.event_handler = {
            event_logic.OriginalDataLoaded: self.original_data_loaded_action,
            event_logic.SegmentationUpdated: self.segmentation_updated_action,
            event_logic.ClearAllEvent: self.clear_all_action
        }

    # region handle events

    def original_data_loaded_action(self, event: event_logic.OriginalDataLoaded):
        self.child_widget.set_original_data(original_data=event.original_data)

    def segmentation_updated_action(self, event: event_logic.SegmentationUpdated):
        LOGGER.info(f'Handle {event.event_name}.')
        self.child_widget.set_components_3d(event.regions_3d)
        if self.stage_enabled:
            LOGGER.info(f'{self.stage_name} is {self.stage_enabled}')
            self.child_widget.refresh()
        else:
            LOGGER.info(f'{self.stage_name} is ready for action.')
            self.stage_enabled_rbtn.setEnabled(True)

    def clear_all_action(self, event: event_logic.ClearAllEvent):
        self.child_widget.clear_all()
        self.child_widget.setEnabled(False)
        self.stage_enabled_rbtn.setEnabled(False)
        self.stage_enabled_rbtn.setChecked(False)

    # endregion handle events

    def stage_enabled_rbtn_click(self):
        if self.stage_enabled_rbtn.isChecked():
            self.stage_enabled = True
            self.child_widget.setEnabled(True)
            self.child_widget.refresh()
        else:
            self.stage_enabled = False
            self.child_widget.setEnabled(False)

    def get_stage_params(self):
        return dict()

    def region_picked(self, defect_data, original_image_region):
        self.send_event(event_logic.RegionPickedEvent(defect_data, original_image_region))

# app = QApplication([])
# widget = DefectSearcherTab()
# widget.show()
# app.exec_()
