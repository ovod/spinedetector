import numpy as np

from spine_detector.utils import timer


@timer
def get_dummy_dispersion_mask(data):
    dispersion_matrix = np.zeros_like(data)
    for x in range(1, data.shape[0] - 1):
        for y in range(1, data.shape[1] - 1):
            for z in range(1, data.shape[2] - 1):
                dispersion_matrix[x][y][z] = np.var(data[x - 1:x + 1, y - 1:y + 1, z - 1:z + 1])
    return dispersion_matrix
