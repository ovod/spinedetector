from spine_detector.constants import LOGGER
from abc import ABC
from typing import List, Type, Dict, Callable, Set, Iterable


class Event(ABC):
    event_name: str

    def __init__(self, publisher=None):
        self.event_name = self.__class__.__name__
        self.publisher = publisher


class LoadDataEvent(Event):
    def __init__(self, source_type: str, path):
        super().__init__()
        self.source_type = source_type
        self.path = path


class RegionPickedEvent(Event):
    def __init__(self, data_with_defects, original_region_data):
        super().__init__()
        self.data_with_defects = data_with_defects
        self.original_region_data = original_region_data


class OriginalDataLoaded(Event):
    def __init__(self, original_data):
        super().__init__()
        self.original_data = original_data


class FilterMaskUpdated(Event):
    def __init__(self, filtered_data):
        super().__init__()
        self.filtered_data = filtered_data


class SliceUpdateEvent(Event):
    position: int

    def __init__(self, new_position):
        super().__init__()
        self.position = new_position

    @property
    def new_position(self):
        return self.position


class FiltrationUpdateEvent(Event):
    def __init__(self, publisher, filtration_params=None, position=0):
        super().__init__()
        if filtration_params is None:
            filtration_params = dict()
        self.filtration_params = filtration_params
        self.position = position
        self.publisher = publisher


class FiltrationEnabledEvent(Event):
    def __init__(self, filtration_params={}, position=0, publisher=None):
        super().__init__()
        self.filtration_params = filtration_params
        self.position = position
        self.publisher = publisher


class SegmentationUpdated(Event):
    def __init__(self, regions_3d):
        super().__init__()
        self.regions_3d = regions_3d


class FiltrationDisabledEvent(Event):
    def __init__(self):
        super().__init__()


class ClearAllEvent(Event):
    def __init__(self):
        super().__init__()


class TabChangedEvent(Event):
    def __init__(self, new_position):
        super().__init__()
        self.new_position = new_position


class Component:
    _listeners: Set['Component'] = set()
    event_handler: Dict[Type['Event'], Callable] = {}

    def set_listeners(self, listeners: Iterable):
        self._listeners = set(listeners)

    def add_listener(self, new_listener):
        self._listeners.add(new_listener)

    def handle_event(self, event: Event):
        self.event_handler.get(type(event), self.default_handler)(event)

    def default_handler(self, event):
        LOGGER.info(f'Event {event.event_name} not supported in component {self.__class__.__name__}')

    def send_event(self, event: Event):
        for listener in self._listeners:
            listener.handle_event(event)
