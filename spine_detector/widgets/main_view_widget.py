from spine_detector.constants import LOGGER

from PyQt5 import QtWidgets, QtCore

from spine_detector import event_logic
from spine_detector.utils import get_default_image_data, colorize_gray_image
from spine_detector.widgets.common.custom_image_view import CustomImageView
from spine_detector.widgets.common.parameter_slider_widget import ParameterSliderWidget
import numpy as np

slice_slider_spec = dict(
    name='slice_slider',
    label='Slice',
    min_value=1,
    max_value=15,
    init_value=1
)


class UI_MainView(object):
    def setupUi(self, main_view, slice_slider_options):
        # main_view.setObjectName("parameter_slider")
        main_view.resize(400, 900)
        main_view.setMinimumSize(QtCore.QSize(400, 900))
        main_view.setMaximumSize(QtCore.QSize(400, 900))

        self.vertical_layout = QtWidgets.QVBoxLayout(main_view)

        self.original_image = CustomImageView(main_view)
        self.vertical_layout.addWidget(self.original_image)

        self.slice_slider = ParameterSliderWidget(main_view, **slice_slider_options)
        self.slice_slider.setEnabled(False)
        self.vertical_layout.addWidget(self.slice_slider)

        self.stage_image = CustomImageView(main_view, name='main_image', connect_to=None)
        self.vertical_layout.addWidget(self.stage_image)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.vertical_layout.addItem(spacerItem)


class MainViewWidget(QtWidgets.QWidget, UI_MainView, event_logic.Component):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self, slice_slider_spec)

        self.slice_slider.parameter_slider.sliderReleased.connect(self.slice_position_released)

        # internal
        self.original_data: np.ndarray = None
        self.filtered_mask: np.ndarray = None
        self.segmentation_data: np.array = None
        self.slices_range = (None, None)

        self.defected_region: np.ndarray = None
        self.original_region_data: np.ndarray = None
        self.region_range = (None, None)
        self.value_dropped = False

        self.current_stage: int = 0

        self.event_handler = {
            event_logic.OriginalDataLoaded: self.original_data_loaded_action,
            event_logic.SegmentationUpdated: self.segmentation_updated,
            event_logic.FilterMaskUpdated: self.filter_mask_updated_action,
            event_logic.RegionPickedEvent: self.region_picked_action,
            event_logic.TabChangedEvent: self.tab_changed_action,
            event_logic.ClearAllEvent: self.clear_all_action
        }

    def slice_position_released(self):
        position = self.slice_slider.value()
        LOGGER.info(f'Slice position changed to position {position}')
        self.send_event(event_logic.SliceUpdateEvent(position - 1))
        self.refresh()

    # region event actions
    def original_data_loaded_action(self, event: event_logic.OriginalDataLoaded):
        self.set_original_data(event.original_data)
        self.slices_range = (1, event.original_data.shape[0])
        #self.slice_slider.update_range(*self.slices_range)
        self.slice_slider.setEnabled(True)

    def filter_mask_updated_action(self, event: event_logic.FilterMaskUpdated):
        self.filtered_mask = event.filtered_data
        self.refresh()

    def segmentation_updated(self, event: event_logic.SegmentationUpdated):
        self.segmentation_data = np.zeros_like(self.original_data, dtype=np.bool)
        for region in event.regions_3d:
            self.segmentation_data[region.slice] += region.filled_image
        self.refresh()

    def region_picked_action(self, event: event_logic.RegionPickedEvent):
        self.defected_region = event.data_with_defects
        self.original_region_data = event.original_region_data
        self.region_range = (1, event.data_with_defects.shape[0])
        self.refresh()

    def tab_changed_action(self, event: event_logic.TabChangedEvent):
        self.current_stage = event.new_position
        self.refresh()

    def clear_all_action(self, event: event_logic.ClearAllEvent):
        self.clear()

    # region event actions
    def set_original_data(self, original_data):
        self.original_data = original_data
        self.refresh()

    def update_stage_image(self, stage_data):
        pass

    def refresh(self):
        if type(self.original_data) is not np.ndarray:
            return
        position = self.slice_slider.value()
        self.original_image.set_image(self.original_data[position - 1])
        if self.current_stage == 0:
            if type(self.filtered_mask) is np.ndarray:
                self.slice_slider.update_range(*self.slices_range)
                original_image = self.original_data[position - 1].copy()

                filter_mask = self.filtered_mask[position - 1].astype(bool)
                original_image[filter_mask] = np.maximum(20, original_image[filter_mask])
                colored_image = colorize_gray_image(original_image,
                                                    filter_mask,
                                                    hue=0.4, saturation=1)
                self.stage_image.set_image(colored_image)
                #self.stage_image.set_image(original_image * (~self.filtered_mask[position - 1].astype(bool)))
                self.defected_region = None
                self.value_dropped = False
        elif self.current_stage == 1:
            if type(self.segmentation_data) is np.ndarray:
                self.slice_slider.update_range(*self.slices_range)
                original_image = self.original_data[position - 1].copy()
                segmentation_mask = self.segmentation_data[position - 1].astype(bool)
                original_image[segmentation_mask] = np.maximum(20, original_image[segmentation_mask])
                colored_image = colorize_gray_image(original_image,
                                                    segmentation_mask,
                                                    hue=0.45, saturation=1)
                self.stage_image.set_image(colored_image)
                self.defected_region = None
                self.value_dropped = False
        elif self.current_stage == 2:
            if type(self.defected_region) is np.ndarray:
                self.slice_slider.update_range(*self.region_range)
                if not self.value_dropped:
                    self.value_dropped = True
                    self.slice_slider.set_value(1)
                region_position = self.slice_slider.value()
                self.original_image.set_image(self.original_region_data[region_position - 1])
                self.stage_image.set_image(self.defected_region[region_position - 1])

    def clear(self):
        self.original_data = None
        self.filtered_mask = None
        self.original_image.set_image(get_default_image_data())
        self.stage_image.set_image(get_default_image_data())
        self.slice_slider.set_value(0)
        self.slice_slider.setEnabled(False)

#
# app = QApplication(sys.argv)
# widget = MainViewWidget(None)
# widget.show()
# app.exec_()
