from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from spine_detector.constants import LOGGER
from spine_detector.domain_classes import ComponentInfo


class ComponentsListWidget(QtWidgets.QListWidget):
    def __init__(self, parent):
        super(ComponentsListWidget, self).__init__(parent=parent)
        self.itemClicked.connect(self.item_click_action)
        self.prev_label = -1

    def item_click_action(self, item):
        if item.text() == self.prev_label:
            self.prev_label = str(-1)
            item.setSelected(False)
            self.item_unselect_action(item)
            return
        self.item_select_action(item)
        self.prev_label = item.text()

    def item_unselect_action(self, item):
        LOGGER.debug(f'Item with label {item.text()} unselected')

    def item_select_action(self, item):
        LOGGER.debug(f'Item with label {item.text()} selected')


class QCustomQWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, vert_info: ComponentInfo = None):
        super(QCustomQWidget, self).__init__(parent=parent)
        self.textQVBoxLayout = QtWidgets.QVBoxLayout()
        self.main_label = QtWidgets.QLabel(vert_info.as_json())
        self.textQVBoxLayout.addWidget(self.main_label)
        self.allQHBoxLayout = QtWidgets.QHBoxLayout()
        self.iconQLabel = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.setLayout(self.allQHBoxLayout)


class StructureInfoView(QtWidgets.QWidget):
    def __init__(self, vert_info: ComponentInfo, parent=None):
        super(StructureInfoView, self).__init__(parent)
        vals = vert_info.to_structure_info_view()
        vbox = QtWidgets.QVBoxLayout(self)

        volume_label = QtWidgets.QLabel('Volume')
        volume_value = QtWidgets.QLabel(vals['volume'])
        tmp_hbox = QtWidgets.QHBoxLayout()
        tmp_hbox.addWidget(volume_label)
        tmp_hbox.addWidget(volume_value)
        vbox.addLayout(tmp_hbox)

        centroid_label = QtWidgets.QLabel('Center')
        centroid_value = QtWidgets.QLabel(vals['center'])
        tmp_hbox = QtWidgets.QHBoxLayout()
        tmp_hbox.addWidget(centroid_label)
        tmp_hbox.addWidget(centroid_value)
        vbox.addLayout(tmp_hbox)

        start_stop_label = QtWidgets.QLabel('Start/Stop')
        start_stop_value = QtWidgets.QLabel(vals['start_stop'])
        tmp_hbox = QtWidgets.QHBoxLayout()
        tmp_hbox.addWidget(start_stop_label)
        tmp_hbox.addWidget(start_stop_value)
        vbox.addLayout(tmp_hbox)


