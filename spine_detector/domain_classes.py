from dataclasses import dataclass, asdict
from enum import Enum

from skimage.measure._regionprops import RegionProperties


class SliderType(Enum):
    GENERAL = 'general'
    ADVANCED = 'advanced'


class FilterTypes(Enum):
    GAUSS_BASED = 'gauss_based'
    EDGE_FILTER = 'edge_filter'
    THRESHOLD_FILTER = 't_filter'
    ADAPTIVE_THRESHOLD_FILTER = 'adaptive_t_filter'
    GRADIENT_FILTER = 'gradient_filter'
    LOCAL_THRESHOLD_FILTER = 'local_threshold_filter'


@dataclass
class ComponentInfo:
    region: RegionProperties
    label: int
    center: tuple
    volume: str = ""
    problems: str = ""

    @classmethod
    def from_region(cls, region: RegionProperties):
        return ComponentInfo(
            region=region,
            label=region.label,
            center=region.centroid,
            volume=region.filled_area
        )

    def to_structure_info_view(self):
        return dict(
            volume=str(self.region.filled_area),
            center=f"x={round(self.center[0])}, y={round(self.center[1])} z={round(self.center[2])}",
            start_stop=f"{self.region.slice[0].start}-{self.region.slice[0].stop}"
        )

    def as_dict(self):
        return asdict(self)

    def as_json(self):
        center = (round(self.center[0]), round(self.center[1]), round(self.center[2]))
        slices_range = self.region.slice
        return f"""
        label: {self.label},
        center: {center},
        volume: {self.volume}
        slices_info: {slices_range}
        """

    def __str__(self):
        return str(asdict(self))
