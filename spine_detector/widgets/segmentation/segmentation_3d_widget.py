from typing import List, Tuple

from PyQt5 import QtWidgets
import pyqtgraph.opengl as gl
import numpy as np
from PyQt5.QtWidgets import QApplication
from pyqtgraph import Vector
from pyqtgraph.opengl import GLViewWidget
from skimage.color import label2rgb

from spine_detector.utils import get_3d_model


class UI_SegmentationScene3d(object):
    def setupUi(self, segmentation_3d_widget, width, height, margin=10):
        segmentation_3d_widget.resize(width, height)
        segmentation_3d_widget.setMinimumSize(width, height)
        segmentation_3d_widget.setMaximumSize(width, height)

        self.central_layout = QtWidgets.QVBoxLayout(segmentation_3d_widget)

        self.view_3d_label = QtWidgets.QLabel(segmentation_3d_widget)
        self.view_3d_label.setText('View in 3D')
        self.central_layout.addWidget(self.view_3d_label)

        self.segmentation_view = GLViewWidget(segmentation_3d_widget)
        self.segmentation_view.setMinimumSize(width - 2 * margin, width - 2 * margin)
        self.segmentation_view.setMaximumSize(width - 2 * margin, width - 2 * margin)

        self.central_layout.addWidget(self.segmentation_view)

        self.change_state_rbt = QtWidgets.QRadioButton(self)
        self.change_state_rbt.setText('Show in color')
        self.change_state_rbt.setChecked(False)
        self.change_state_rbt.setAutoExclusive(False)

        self.central_layout.addWidget(self.change_state_rbt)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.central_layout.addItem(spacerItem)


class SegmentationScene3dWidget(QtWidgets.QWidget, UI_SegmentationScene3d):
    raw_data: np.ndarray

    # elements
    segmentation_view: GLViewWidget

    def __init__(self, parent, width=400, height=800):
        super().__init__(parent=parent)
        self.setupUi(self, width, height)

        self.segmentation_view.opts['distance'] = 512
        self.segmentation_view.getViewport()
        # ax = gl.GLAxisItem()
        # self.segmentation_view.addItem(ax)
        self._original_volume_item = None
        self.colorized = False
        self.regions_3d = None

        self.change_state_rbt.clicked.connect(self.change_colored_state)

    def change_colored_state(self):
        if self.change_state_rbt.isChecked():
            self.colorized = True
        else:
            self.colorized = True
        self.remove_data()
        self.update_components(self.regions_3d)

    def update_components(self, regions_3d, k=5, alpha=0.5):
        if not regions_3d:
            return
        self.regions_3d = regions_3d
        centroids = np.array([np.array(region.centroid) for region in regions_3d])
        center = np.mean(centroids, axis=0).astype(np.uint8)

        original_data = np.zeros_like(regions_3d[0]._label_image, dtype=np.uint8)
        for region in regions_3d:
            original_data[region.slice] += region.filled_image.astype(np.uint8) * region.label

        if self.colorized:
            new_data = np.zeros((*original_data.shape[:3], 4))
            labeled_data = (label2rgb(original_data, bg_label=0) * 255).astype(np.uint8)
            # original_data = (label2rgb(original_data, bg_label=0) * 255).astype(np.uint8)
            new_data[:, :, :, :3] = labeled_data
            alpha_array = new_data[:, :, :, 3]
            alpha_array[original_data != 0] = 180
            new_data[:, :, :, 3] = alpha_array
            original_data = new_data
        else:
            original_data = (original_data != 0).astype(np.uint8) * 255

        self.add_data(original_data, k, alpha, center)

    def add_data(self, data, k=5, alpha=0.5, center=None):
        prepared_data = get_3d_model(data, k=k, alpha=alpha)

        if type(center) is np.ndarray:
            x, y, z = center
        else:
            x, y, z = np.array([*prepared_data.shape[:3]])

        self._original_volume_item = gl.GLVolumeItem(prepared_data)
        self.segmentation_view.opts['center'] = Vector(x, z, y)

        self.segmentation_view.addItem(self._original_volume_item)

    def remove_data(self):
        if self._original_volume_item:
            self.segmentation_view.removeItem(self._original_volume_item)

    def change_scene_center(self, new_position: tuple):
        # self._original_volume_item.translate()
        self.segmentation_view.update()

    def reset_center_to_default(self):
        # self.segmentation_view.opts['center'] = Vector(0, 0, 0)
        self.segmentation_view.update()
