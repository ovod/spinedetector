from PyQt5 import QtCore, QtWidgets
import numpy as np
from spine_detector.constants import LOGGER

from spine_detector.domain_classes import SliderType


class UI_ParameterSlider(object):
    def setupUi(self, parameter_slider):
        parameter_slider.setFixedSize(QtCore.QSize(350, 30))
        self.horizontalLayout = QtWidgets.QHBoxLayout(parameter_slider)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.parameter_label = QtWidgets.QLabel(parameter_slider)
        self.parameter_label.setMinimumSize(70, 25)
        self.parameter_label.setMaximumSize(70, 25)
        self.parameter_label.setObjectName("parameter_label")
        self.horizontalLayout.addWidget(self.parameter_label)

        self.parameter_value = QtWidgets.QLabel(parameter_slider)
        self.parameter_value.setMinimumSize(40, 25)
        self.parameter_value.setMaximumSize(40, 25)

        self.parameter_value.setObjectName("parameter_value")
        self.horizontalLayout.addWidget(self.parameter_value)
        self.parameter_slider = QtWidgets.QSlider(parameter_slider)
        self.parameter_slider.setOrientation(QtCore.Qt.Horizontal)
        self.parameter_slider.setObjectName("horizontalSlider")
        self.parameter_slider.setMaximumSize(220, 25)
        self.parameter_slider.setMinimumSize(220, 25)
        self.horizontalLayout.addWidget(self.parameter_slider)

        self.retranslateUi(parameter_slider)
        QtCore.QMetaObject.connectSlotsByName(parameter_slider)

    def retranslateUi(self, parameter_slider):
        _translate = QtCore.QCoreApplication.translate
        parameter_slider.setWindowTitle(_translate("parameter_slider", "Form"))
        self.parameter_label.setText(_translate("parameter_slider", "TextLabel"))
        self.parameter_value.setText(_translate("parameter_slider", "TextLabel"))


class ParameterSliderWidget(QtWidgets.QWidget, UI_ParameterSlider):
    def __init__(self, parent, **init_params):
        super().__init__(parent=parent)
        self.setupUi(self)
        self.slider_name = init_params.get('name', 'Default name')
        self.slider_type = init_params.get('slider_type', SliderType.GENERAL)

        self.to_outer_value_f = init_params.get('to_outer_value_f', lambda x: x)
        self.to_inner_value_f = init_params.get('to_inner_value_f', lambda x: x)
        self.slider_label = init_params.get('label', 'Default value')
        self.min_value = init_params.get('min_value', 0)
        self.max_value = init_params.get('max_value', 100)
        self.init_value = init_params.get('init_value', self.min_value)

        self.parameter_label.setText(self.slider_label)
        self.parameter_slider.setMinimum(int(self.to_inner_value_f(self.min_value)))
        self.parameter_slider.setMaximum(int(self.to_inner_value_f(self.max_value)))
        self.parameter_slider.setValue(int(self.to_inner_value_f(self.init_value)))
        self.parameter_value.setText(f'{round(self.to_outer_value_f(self.init_value), 2)}')

        self.parameter_slider.valueChanged.connect(self.parameter_value_on_change)
        self.parameter_slider.sliderReleased.connect(self.parameter_released)

    def parameter_value_on_change(self):
        value = self.to_outer_value_f(self.parameter_slider.value())
        self.parameter_value.setText(f'{round(value, 2)}')

    def parameter_released(self):
        value = self.to_outer_value_f(self.parameter_slider.value())
        value = round(value, 2)
        LOGGER.debug(f'{self.slider_name}: Parameter change to {value}')

    def value(self):
        return self.to_outer_value_f(self.parameter_slider.value())

    def set_value(self, new_value):
        self.parameter_slider.setValue(int(self.to_inner_value_f(new_value)))
        self.parameter_value.setText(str(new_value))

    def update_spec(self):
        pass

    def update_range(self, min_value, max_value, drop_value=False):
        self.parameter_slider.setMinimum(min_value)
        self.parameter_slider.setMaximum(max_value)
        if drop_value:
            self.set_value(min_value)


class FloatParameterSlider(ParameterSliderWidget):
    def __init__(self, parent=None, **kwargs):
        kwargs['to_inner_value_f'] = lambda x: x * 100
        kwargs['to_outer_value_f'] = lambda x: x / 100
        super().__init__(parent=parent, **kwargs)
        if kwargs.get('init_value'):
            self.parameter_value.setText(str(kwargs['init_value']))


class LogParameterSlider(ParameterSliderWidget):
    def __init__(self, parent=None, **kwargs):
        kwargs['to_inner_value_f'] = lambda x: np.log(x) * 2 + 100
        kwargs['to_outer_value_f'] = lambda x: np.exp((x - 100) / 2)
        super().__init__(parent, **kwargs)


