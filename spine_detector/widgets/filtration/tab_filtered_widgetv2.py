from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication

from spine_detector.constants import LOGGER
from spine_detector.domain_classes import SliderType
from spine_detector.event_logic import Component, OriginalDataLoaded, SliceUpdateEvent, ClearAllEvent, FilterMaskUpdated
from spine_detector.widgets.filtration.common_filtration_widget import FilterControlWidget


class FilterTabWidgetV2(QtWidgets.QWidget, Component):

    def setup_ui(self):
        self.setFixedSize(1240, 820)

        central_layout = QtWidgets.QVBoxLayout(self)
        btns_vertical_layout = QtWidgets.QHBoxLayout(self)
        self.filtration_enable_rbtn = QtWidgets.QRadioButton(self)
        self.filtration_enable_rbtn.setText('Filtration enabled')
        self.filtration_enable_rbtn.setAutoExclusive(False)
        self.filtration_enable_rbtn.setEnabled(False)
        btns_vertical_layout.addWidget(self.filtration_enable_rbtn)

        # self.advanced_options_enable_rbtn = QtWidgets.QRadioButton(self)
        # self.advanced_options_enable_rbtn.setText('Advanced options')
        # self.advanced_options_enable_rbtn.setAutoExclusive(False)
        # self.advanced_options_enable_rbtn.setEnabled(False)
        # btns_vertical_layout.addWidget(self.advanced_options_enable_rbtn)

        spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        btns_vertical_layout.addItem(spacer)
        central_layout.addLayout(btns_vertical_layout)
        self.common_filter_widget = FilterControlWidget(self)
        central_layout.addWidget(self.common_filter_widget)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setup_ui()

        self.stage_name = 'filtration'

        self.current_slice_position: int = 0
        self.filtration_enabled = False
        self.adv_options_enabled = False
        # events
        self.event_handler = {
            OriginalDataLoaded: self.original_data_loaded_action,
            SliceUpdateEvent: self.slice_update_action,
            ClearAllEvent: self.clear_all_action
        }
        # btns
        self.filtration_enable_rbtn.clicked.connect(self.change_filtration_state)

    # region handle events
    def original_data_loaded_action(self, event: OriginalDataLoaded):
        self.set_original_data(event.original_data)
        self.filtration_enable_rbtn.setEnabled(True)
        if self.filtration_enabled:
            self.refresh()
            for filter_widget in self.filters_map.values():
                filter_widget.is_used_rbtn.setEnabled(True)

    def slice_update_action(self, event: SliceUpdateEvent):
        self.slice_update(event.position)

    def clear_all_action(self, event: ClearAllEvent):
        self.clear_all()

    # endregion

    # region btns connectors

    def change_filtration_state(self):
        if self.filtration_enable_rbtn.isChecked():
            LOGGER.info('Filtration enabled')
            self.filtration_enabled = True

            self.common_filter_widget.set_activated(True)
            self.common_filter_widget.refresh()
        else:
            LOGGER.info('Filtration disabled')
            self.filtration_enabled = False
            self.adv_options_enabled = False

            self.common_filter_widget.set_activated(False)

    # endregion

    def set_original_data(self, original_data):
        self.common_filter_widget.set_original_data(original_data)

    def slice_update(self, new_position):
        self.common_filter_widget.slice_update(new_position)

    def clear_all(self):
        self.common_filter_widget.clean_all()
        self.filtration_enable_rbtn.setEnabled(False)

    def result_mask_update(self, result_mask):
        self.send_event(FilterMaskUpdated(result_mask))

    def get_stage_params(self):
        return {}

# app = QApplication([])
# widget = FilterTabWidgetV2()
# widget.setWindowTitle('FilterTabWidget')
# widget.show()
# app.exec_()

