import numpy as np

from spine_detector.event_logic import OriginalDataLoaded
from spine_detector.utils import load_mri, rescale_original_image


def send_load_data_event(component, path='test-data/0001/T2_TSE_SAG_384_0002'):
    original_data = np.array([rescale_original_image(array.pixel_array) for array in load_mri(path)])
    component.send_event(OriginalDataLoaded(original_data))
