from spine_detector.constants import LOGGER
from typing import Callable

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt

from spine_detector.widgets.common.list_items import ComponentsListWidget, QCustomQWidget, StructureInfoView


class SegmentationInfoWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SegmentationInfoWidget, self).__init__(parent=parent)
        self.setMinimumSize(400, 800)
        self.setMaximumSize(400, 800)

        self.central_layout = QtWidgets.QVBoxLayout(self)

        self.list_label = QtWidgets.QLabel(self)
        self.list_label.setText('Components:')
        self.central_layout.addWidget(self.list_label)

        self.components_list = ComponentsListWidget(self)
        self.components_list.setMinimumSize(380, 380)
        self.components_list.setMaximumSize(380, 380)
        self.central_layout.addWidget(self.components_list)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.central_layout.addItem(spacerItem)

        self.post_init()

    def post_init(self):
        pass

    def change_item_click_handler(self, new_handler: Callable):
        self.components_list.itemClicked.disconnect()
        self.components_list.itemClicked.connect(new_handler)

    def add_items(self, *components):
        for component in components:
            item_view_widget = StructureInfoView(component)
            list_item = QtWidgets.QListWidgetItem(self.components_list)
            list_item.setSizeHint(item_view_widget.sizeHint())
            list_item.setData(Qt.UserRole, component)
            list_item.setText(f'{component.label}')
            self.components_list.addItem(list_item)
            self.components_list.setItemWidget(list_item, item_view_widget)

    def delete_all_items(self):
        self.components_list.clear()
