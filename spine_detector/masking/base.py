import numpy as np

from spine_detector.utils import timer


def _min_max(value, abs_min, abs_max, rmin=0, rmax=255):
    std = (value - abs_min)/(abs_max - abs_min)
    scaled = std * (rmin - rmax) + rmin
    return scaled

@timer
def filter_by_mask(data, mask, k_min=0, k_max=255):
    mask_max = np.max(mask)
    coef_max = _min_max(k_min, 0, mask_max)
    coef_min = _min_max(k_max, 0, mask_max)

    filtered_mask = np.invert((mask <= coef_max) & (mask >= coef_min))

    masked_data = data * filtered_mask

    return masked_data
