from enum import Enum
from typing import Dict, Callable

from PyQt5 import QtWidgets, QtGui
from PyQt5.QtCore import Qt
from matplotlib import pyplot
from pyqtgraph.opengl import GLViewWidget, GLAxisItem, GLVolumeItem
import numpy as np

from spine_detector.constants import LOGGER
from spine_detector.utils import get_3d_model


class GroupBox(QtWidgets.QWidget):

    def __init__(self, group_type: str, group_name: str, spec, parent=None):
        super(GroupBox, self).__init__(parent=parent)

        layout = QtWidgets.QGridLayout()
        self.setLayout(layout)

        groupbox = QtWidgets.QGroupBox(group_name)
        self.group_type = group_type
        layout.addWidget(groupbox)

        vbox = QtWidgets.QHBoxLayout()
        groupbox.setLayout(vbox)

        self.btns_list = []
        for btn_text, enum_val in spec:
            radio_btn = QtWidgets.QRadioButton(btn_text)
            radio_btn.__setattr__('enum_val', enum_val)
            radio_btn.clicked.connect(self.click)
            self.btns_list.append(radio_btn)
            vbox.addWidget(radio_btn)
        vbox.addItem(QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))

        self.btns_list[0].setChecked(True)

    def click(self):
        clicked_on = next(x for x in self.btns_list if x.isChecked())
        if self.parent():
            self.parent().change_state(self.group_type, clicked_on.enum_val)


class DefectViewMode(Enum):
    ORIGINAL_IMAGE = 0
    BINARY_IMAGE = 1
    GRADIENT_IMAGE = 2


data_view_map = [
    ('Original data', DefectViewMode.ORIGINAL_IMAGE),
    ('Binary data', DefectViewMode.BINARY_IMAGE),
    ('Gradient data', DefectViewMode.GRADIENT_IMAGE),
]


class MaskBehaviorMode(Enum):
    WITHOUT_MASK = 0
    WITH_MASK = 1


mask_mode = [
    ('With mask', MaskBehaviorMode.WITH_MASK),
    ('Without mask', MaskBehaviorMode.WITHOUT_MASK)
]


class ShowDefectsMode(Enum):
    WITHOUT_DEFECTS = 0
    WITH_DEFECTS = 1


defects_mode = [
    ('Without defects', ShowDefectsMode.WITHOUT_DEFECTS),
    ('With defects', ShowDefectsMode.WITH_DEFECTS)
]


class DefectView3d(QtWidgets.QWidget):
    def setup_ui(self):
        view_3d_layout = QtWidgets.QVBoxLayout(self)
        view_3d_label = QtWidgets.QLabel('View 3D')
        view_3d_layout.addWidget(view_3d_label, 0, Qt.AlignTop)
        self.view_3d = GLViewWidget(self)
        self.view_3d.setFixedSize(640, 400)
        view_3d_layout.addWidget(self.view_3d, 0, Qt.AlignTop)
        self.data_mode_box = GroupBox('data_mode', 'Data mode', data_view_map, self)
        view_3d_layout.addWidget(self.data_mode_box)
        self.mask_mode_box = GroupBox('mask_mode', 'Mask mode', mask_mode, self)
        view_3d_layout.addWidget(self.mask_mode_box)
        self.defect_mode_box = GroupBox('defect_mode', 'Defect mode', defects_mode, self)
        view_3d_layout.addWidget(self.defect_mode_box)

        view_3d_layout.addItem(
            QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))

    def __init__(self, parent=None):
        super(DefectView3d, self).__init__(parent=parent)
        self.setup_ui()
        self.view_3d.opts['distance'] = 512
        self.volume_item = None
        ax = GLAxisItem(size=QtGui.QVector3D(100, 100, 100))
        self.view_3d.addItem(ax)
        # default_state
        self.data_mode_state = self.data_mode_box.btns_list[0].enum_val
        self.mask_mode_state = self.mask_mode_box.btns_list[0].enum_val
        self.defect_show_state = self.defect_mode_box.btns_list[0].enum_val
        #
        self.original_data: np.ndarray = None
        self.binary_data: np.ndarray = None
        self.gradient_data: np.ndarray = None
        self.defect_data: np.ndarray = None

        self.data_mode_strategy: Dict[DefectViewMode, Callable] = {
            DefectViewMode.ORIGINAL_IMAGE: self.get_original_data_strategy,
            DefectViewMode.BINARY_IMAGE: self.get_binary_data_strategy,
            DefectViewMode.GRADIENT_IMAGE: self.get_gradient_data_strategy
        }

        self.mask_mode_strategy: Dict[MaskBehaviorMode, Callable] = {
            MaskBehaviorMode.WITHOUT_MASK: self.without_mask_strategy,
            MaskBehaviorMode.WITH_MASK: self.with_mask_strategy
        }

        self.defect_mode_strategy: Dict[ShowDefectsMode, Callable] = {
            ShowDefectsMode.WITHOUT_DEFECTS: self.withoud_defects,
            ShowDefectsMode.WITH_DEFECTS: self.with_defects
        }

    # region strategy
    def get_original_data_strategy(self):
        return self.original_data

    def get_binary_data_strategy(self):
        return self.binary_data.astype(np.uint8) * 255

    def get_gradient_data_strategy(self):
        if type(self.gradient_data) is np.ndarray:
            # rgba_data = gray2rgb(self.gradient_data, True)
            jet_cmap = pyplot.get_cmap('bone')
            self.gradient_data[self.gradient_data > 50] = 50.
            norm_gradient_data = (self.gradient_data - self.gradient_data.min())/self.gradient_data.max()
            gradient_data = jet_cmap(norm_gradient_data) * 255
            gradient_data[:, :, :, 3] = 127
            return gradient_data
        return self.gradient_data

    def without_mask_strategy(self, data_for_filter):
        return data_for_filter

    def with_mask_strategy(self, data_for_filter):
        if len(data_for_filter.shape) == 4:
            binary_mask_4d = np.stack((self.binary_data,) * 4, axis=-1)
            return data_for_filter * binary_mask_4d
        return data_for_filter * self.binary_data

    def withoud_defects(self, data_for_show_defects):
        return data_for_show_defects

    def with_defects(self, data_for_show_defects):
        return self.defect_data

    # endregion strategy

    def update_data_v2(self, original_data, binary_data, gradient_data, defect_data=None):
        self.original_data = original_data
        self.binary_data = binary_data
        self.gradient_data = gradient_data
        self.defect_data = defect_data

        self.refresh()

    def change_state(self, mode_type, mode_state):
        LOGGER.info('Mode changed')
        if mode_type is 'data_mode':
            self.data_mode_state = mode_state
        elif mode_type is 'mask_mode':
            self.mask_mode_state = mode_state
        else:
            self.defect_show_state = mode_state

        self.refresh()

    def refresh(self):
        result_data = self.data_mode_strategy[self.data_mode_state]()
        if type(result_data) is np.ndarray:
            if self.volume_item:
                self.view_3d.removeItem(self.volume_item)
            result_data = self.mask_mode_strategy[self.mask_mode_state](result_data)
            result_data = self.defect_mode_strategy[self.defect_show_state](result_data)

            prepared_data = get_3d_model(result_data, k=5, alpha=0.5)
            self.volume_item = GLVolumeItem(prepared_data)
            x, y, z = prepared_data.shape[:3]
            self.volume_item.translate(-x // 2, -y // 2, -z // 2)
            self.view_3d.addItem(self.volume_item)

    def set_data(self, data):
        prepared_data = get_3d_model(data, k=5, alpha=0.5)
        self.volume_item = GLVolumeItem(prepared_data)
        x, y, z = prepared_data.shape[:3]
        #self.volume_item.translate(-x // 2, -y // 2, -z // 2)
        self.view_3d.addItem(self.volume_item)

    def clear_all(self):
        if self.volume_item:
            self.view_3d.removeItem(self.volume_item)