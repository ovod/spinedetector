import collections
import functools

from skimage import color

import numpy as np
from typing import Any, List, Tuple
import os
import pydicom
from PyQt5.QtGui import QImage, qRgb, QPixmap
from pydicom import FileDataset
from datetime import datetime
from spine_detector.constants import LOGGER


def at_least_two_true(arr_1, arr_2, arr_3):
    """
    https://stackoverflow.com/a/3076081
    a && (b || c) || (b && c)
    :param arr_1:
    :param arr_2:
    :param arr_3:
    """
    return np.logical_and(arr_1, np.logical_or(np.logical_or(arr_2, arr_3), np.logical_and(arr_2, arr_3)))


def load_mri_research(research_path) -> Tuple[List[Any], List[Any]]:
    """

    :param research_path: path to directory with mri scans
    :return: list of pixel arrays
    """
    raw_researches: List[FileDataset] = [pydicom.read_file(research_path + '/' + s) for s in os.listdir(research_path)]
    pixel_array_list: List[np.array] = [x.pixel_array for x in raw_researches]
    return pixel_array_list, raw_researches


def load_mri(research_path) -> List[FileDataset]:
    return [pydicom.read_file(research_path + '/' + s) for s in os.listdir(research_path)]


def rescale_original_image(original_pixel_array):
    image_2d = original_pixel_array.astype(float)
    image_2d_scaled = (np.maximum(image_2d, 0) / image_2d.max()) * 255.0
    # image_2d_scaled = np.uint8(image_2d_scaled)
    return image_2d_scaled


def load_npz_file(path_to_npz):
    loaded = np.load(path_to_npz, allow_pickle=True)
    original_data = loaded.get('original_data')
    stage_params = loaded.get('stage_params').item()

    return original_data, stage_params


def timer(func):
    """Print the runtime of the decorated function"""

    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        LOGGER.debug(f'Start {func.__name__}')
        start_time = datetime.now()
        value = func(*args, **kwargs)
        end_time = datetime.now()
        run_time = end_time - start_time
        LOGGER.debug(f"Finished {func.__name__} in {run_time}")
        return value

    return wrapper_timer


@timer
def get_3d_model(raw_array, k=5, alpha=0.5):
    if type(raw_array) != np.ndarray:
        raise ValueError()

    array = raw_array
    array = np.rot90(raw_array, k=3, axes=(1, 2))
    if len(raw_array.shape) == 3:
        # for grayscale
        alpha_array = np.zeros_like(array)
        alpha_array[array != 0] = 255 * alpha
        array = np.stack((array,) * 4, axis=-1)
        array[:, :, :, 3] = alpha_array
        if k == 0:
            return array
        result_array = np.zeros((k * array.shape[0], *array.shape[-3:]))
        for i in range(array.shape[0]):
            result_array[k*i] = array[i]
        return result_array
    elif len(array.shape) == 4:
        result_array = np.zeros((k * array.shape[0], *array.shape[-3:]))
        for i in range(array.shape[0]):
            result_array[k*i] = array[i]
        return result_array


def colorize_gray_image(gray_image, mask, hue, saturation=0.5):
    rgb_image = color.gray2rgb(gray_image)
    return colorize(rgb_image, mask, hue, saturation)


def colorize(rgb_image, mask, hue, saturation=0.5):
    hsv_image = color.rgb2hsv(rgb_image)
    hue_values = hsv_image[:, :, 0]
    hue_values[mask] = hue
    hsv_image[:, :, 0] = hue_values

    saturation_values = hsv_image[:, :, 1]
    saturation_values[mask] = saturation
    hsv_image[:, :, 1] = saturation_values

    result = color.hsv2rgb(hsv_image)

    return result


def prepare_for_view(raw_array: np.array, position: int, k: int = 5, alpha=0.5) -> np.array:
    """
    :param alpha:
    :param raw_array: входной снимко
    :param position: позиция снимка в последовательности
    :param threshold: порог для отображения пикселя
    :param k: коэффициент для корректного отображения позции на снимке
    :return: (100, 320, 320, 4) массов со снимком
    """
    new = np.zeros((100, raw_array.shape[0], raw_array.shape[1], 4), dtype=np.uint8)
    for i in range(raw_array.shape[0]):
        for j in range(raw_array.shape[1]):
            val = raw_array[i][j]
            new[position * k][j][raw_array.shape[0] - i - 1] = np.array([val, val, val, int(255 * alpha)])
    return new


def get_model(filtered_data, k=5, alpha=0.5):
    model = np.zeros((100, filtered_data.shape[1], filtered_data.shape[2], 4), dtype=np.uint8)
    for i in range(filtered_data.shape[0]):
        model += prepare_for_view(filtered_data[i], i, k=k, alpha=alpha)
    return model


def to_qimage(image, copy=True):
    gray_color_table = [qRgb(i, i, i) for i in range(256)]
    qim = QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QImage.Format_Indexed8)
    qim.setColorTable(gray_color_table)
    return qim.copy() if copy else qim


def to_qimage_with_color(image, color=None, copy=True):
    if color == 'r':
        color_table = [qRgb(i, 0, 0) for i in range(256)]
    elif color == 'g':
        color_table = [qRgb(0, i, 0) for i in range(256)]
    elif color == 'b':
        color_table = [qRgb(0, 0, i) for i in range(256)]
    else:
        color_table = [qRgb(i, i, i) for i in range(256)]
    qim = QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QImage.Format_Indexed8)
    qim.setColorTable(color_table)
    return qim.copy() if copy else qim


def get_default_image() -> QPixmap:
    return QPixmap.fromImage(to_qimage(np.zeros((320, 320))))


def get_default_image_data():
    return np.zeros((320, 320))


def get_fake_image_data(k=0, fake_type='sin'):
    if fake_type is 'sin':
        N = 256
        x = np.linspace(-np.pi, np.pi, N)
        sine1D = 128.0 + (127.0 * np.sin(x)) * (k + 1) / 10
        sine1D = np.uint8(sine1D)
        sine2D = np.ndarray((N, N), dtype=np.uint8)
        for i in range(N):
            sine2D[i] = np.roll(sine1D, -i)
        return sine2D


# JUST FOR LULZ
def get_surfin_bird():
    return QPixmap("C:/Users/kozic/Desktop/peter.jpg")


def recursive_update(original_dict, new_params):
    for k, v in new_params.items():
        if isinstance(v, collections.abc.Mapping):
            original_dict[k] = recursive_update(new_params.get(k, {}), v)
        else:
            original_dict[k] = v
    return original_dict
