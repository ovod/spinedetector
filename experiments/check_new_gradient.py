import numpy as np
import matplotlib.pyplot as plt

from spine_detector.masking.gradient import convole, get_gradient
from spine_detector.utils import load_mri_research, rescale_original_image, prepare_for_view

filtered_data = np.zeros((15, 320, 320))
base_result_path = '../results'

first_patient_path = '../test-data/0001'
T1_research = f'{first_patient_path}/T1_TSE_SAG_320_0003'

pixel_arrays, _ = load_mri_research(T1_research)

print(f'Stack data')
original_data = np.zeros((15, 320, 320))

for i, array in enumerate(pixel_arrays):
    original_data[i] = array

normalized_data = rescale_original_image(original_data) / 255
size = (3, 3, 3)
coef = 255 / 6 / 8 / size[0] / size[1] / size[2] * 40
alpha = 1


filtered_data = convole(get_gradient(normalized_data, alpha), size)
filtered_data = filtered_data * coef

# result = np.zeros((100, 320, 320, 4))
#
# for l in range(len(original_data)):
#     result += prepare_for_view(filtered_data[l], position=l)
#
# np.savez_compressed(f'{base_result_path}/gradient_mask_4', data=result)
k = 35
slice_number = 3

tst_image = original_data[slice_number]
tst_mask = filtered_data[slice_number]

fig, ax = plt.subplots(ncols=3, figsize=(15, 5))

ax[0].imshow(tst_image, cmap='gray')

ax[1].imshow(tst_mask, cmap='gray')

ax[2].imshow(tst_image * (tst_mask < k), cmap='gray')

plt.show()
