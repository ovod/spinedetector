from skimage.filters import threshold_otsu
from skimage.morphology import closing, square
from skimage.segmentation import clear_border

from spine_detector.utils import timer


@timer
def otsu_with_borders(original_pixel_array):
    """
    Данный делает следующее:
        * высчитывает трешхолд по отцу
        * накладывает маску
        * возращает pixel_array отрабатанный по маске
    :param original_pixel_array:
    :return:
    """
    thresh = threshold_otsu(original_pixel_array)
    bw = closing(original_pixel_array > thresh, square(1))

    # set borders with 5
    bw[:, 5] = True
    bw[:, 315] = True
    bw[5, :] = True
    bw[315, :] = True

    cleared = clear_border(bw)

    cleared_pixel_array = original_pixel_array * cleared

    return cleared_pixel_array