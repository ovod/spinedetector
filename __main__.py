import logging
import sys

from PyQt5.QtWidgets import QApplication

from spine_detector.constants import add_console_handler, set_log_level

from spine_detector.main import build_window

# prepare logging
add_console_handler()
set_log_level(logging.DEBUG)

app = QApplication(sys.argv)
main_window = build_window()
main_window.setWindowTitle('SpineDetector')
main_window.show()

# main_window._open_mri_dir('test-data/0004/T2_TSE_SAG_384_0002')
# main_window.filtration_tab_widget.filtration_enable_rbtn.click()
# main_window.main_view_widget.slice_slider.set_value(8)
# main_window.main_view_widget.slice_position_released()
# main_window.tabWidget.setCurrentIndex(1)
# main_window.segmentation_tab_widget.segmentation_enable_rbtn.click()

app.exec_()
