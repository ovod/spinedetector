from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from spine_detector.constants import LOGGER
from spine_detector.domain_classes import ComponentInfo
from spine_detector.widgets.common.list_items import ComponentsListWidget, QCustomQWidget


class DefectComponentList(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(DefectComponentList, self).__init__(parent=parent)

        comp_info_layout = QtWidgets.QVBoxLayout(self)
        component_info_label = QtWidgets.QLabel('Components')
        comp_info_layout.addWidget(component_info_label, 0, Qt.AlignTop)
        self.components_list = ComponentsListWidget(self)
        self.components_list.setFixedSize(400, 640)
        comp_info_layout.addWidget(self.components_list, 0, Qt.AlignTop)
        comp_info_layout.addItem(
            QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))

        self.post_init()

    def post_init(self):
        self.prev_label = str(-1)
        self.components_list.itemClicked.connect(self.item_click_action)

    def add_item(self):
        pass

    def add_items(self, components_3d):
        for region in components_3d:
            curr_component = ComponentInfo.from_region(region)
            item_view_widget = QCustomQWidget(None, curr_component)
            list_item = QtWidgets.QListWidgetItem(self.components_list)
            list_item.setSizeHint(item_view_widget.sizeHint())
            list_item.setData(Qt.UserRole, curr_component)
            list_item.setText(f'{curr_component.label}')
            self.components_list.addItem(list_item)
            self.components_list.setItemWidget(list_item, item_view_widget)

    def item_click_action(self, item):
        if item.text() == self.prev_label:
            self.prev_label = str(-1)
            item.setSelected(False)
            self.item_unselect_action(item)
            return
        self.item_select_action(item)
        self.prev_label = item.text()

    def item_unselect_action(self, item):
        LOGGER.debug(f'Item with label {item.text} unselected')

    def item_select_action(self, item):
        LOGGER.debug(f'Item with label {item.text} selected')

    def clear_all(self):
        self.components_list.clear()