import pyqtgraph as pg

from spine_detector.widgets.defects.defect_search_tab import DefectSearcherTab
from spine_detector.widgets.filtration.tab_filtered_widgetv2 import FilterTabWidgetV2
from spine_detector.widgets.main_window_widget import MainWindowWidget
from spine_detector.widgets.segmentation.tab_segmentation_widget_v2 import SegmentationTabWidgetV2

pg.setConfigOptions(imageAxisOrder='row-major')


def build_window(**kwargs):
    filtered_tab_widget = FilterTabWidgetV2()

    segmentation_tab_widget = SegmentationTabWidgetV2()

    defect_searcher_tab = DefectSearcherTab()

    main_window = MainWindowWidget()
    main_window.add_tab_widget(filtered_tab_widget, 'Filtration', 'filtration_tab_widget')
    main_window.add_tab_widget(segmentation_tab_widget, 'Segmentation', 'segmentation_tab_widget')
    main_window.add_tab_widget(defect_searcher_tab, 'Defect searching', 'defect_tab_widget')

    return main_window
