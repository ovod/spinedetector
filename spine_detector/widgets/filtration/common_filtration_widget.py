from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QResizeEvent
from PyQt5.QtWidgets import QApplication

from spine_detector.constants import LOGGER
from spine_detector.domain_classes import FilterTypes, SliderType
from spine_detector.event_logic import Component
from spine_detector.widgets.filtration.filter_merger import FilterMergerWidget
from spine_detector.widgets.filtration.single_filter import SingleFilterWidget, generate_filter_widget
import numpy as np


class FilterControlWidget(QtWidgets.QScrollArea):
    def __init__(self, parent=None):
        super(FilterControlWidget, self).__init__(parent=parent)
        self.setFixedSize(1200, 750)
        self.scroll_area_contents = QtWidgets.QWidget()
        self.scroll_area_contents_layout = QtWidgets.QHBoxLayout(self.scroll_area_contents)
        self.scroll_area_contents_layout.setSpacing(1)
        self.scroll_area_contents_layout.setContentsMargins(0, 0, 0, 0)

        self.filter_merger = FilterMergerWidget(self.scroll_area_contents)
        self.filter_merger.set_controller(self)
        self.filter_merger.setFixedSize(400, 680)
        self.scroll_area_contents_layout.addWidget(self.filter_merger)

        self.filter_widgets = [
            generate_filter_widget(FilterTypes.EDGE_FILTER),
            generate_filter_widget(FilterTypes.ADAPTIVE_THRESHOLD_FILTER),
            generate_filter_widget(FilterTypes.GAUSS_BASED)
        ]

        for filter_widget in self.filter_widgets:
            filter_widget.setParent(self.scroll_area_contents)
            filter_widget.set_controller(self)
            #filter_widget.setFixedSize(400, 680)
            self.scroll_area_contents_layout.addWidget(filter_widget)

        self.gradient_filter = generate_filter_widget(FilterTypes.GRADIENT_FILTER)
        self.gradient_filter.hide()
        self.scroll_area_contents_layout.addWidget(self.gradient_filter)

        self.setWidget(self.scroll_area_contents)
        self.current_position = 0
        self.is_active = False

    def post_init(self):
        pass

        # for filter_widget in self.filter_widgets:
        #     filter_widget.set_parameter_handler(self._sep_filter_params_updated)
        # self.filter_merger.parameters_box.set_parameter_handler(self._result_filter_params_updated)

    def slice_update(self, new_position):
        self.current_position = new_position
        for filter_widget in self.filter_widgets:
            filter_widget.current_position = self.current_position
        self.filter_merger.current_position = self.current_position
        self.gradient_filter.current_position = self.current_position
        if self.is_active:
            for filter_widget in self.filter_widgets:
                filter_widget.refresh()
            self.filter_merger.slice_update()
            self.gradient_filter.refresh()

    def set_activated(self, activation_state):
        if activation_state:
            self.is_active = True
            self.setEnabled(True)
            for filter_widget in self.filter_widgets:
                filter_widget.set_options_state(True, SliderType.GENERAL)
                filter_widget.control_box.setEnabled(True)
            self.filter_merger.set_options_state(True)
            self.filter_merger.control_box.setEnabled(True)
            self.gradient_filter.set_options_state(True, SliderType.GENERAL)
            #self.gradient_filter.set_options_state(True)
        else:
            self.is_active = False
            self.setEnabled(False)
            for filter_widget in self.filter_widgets:
                filter_widget.set_options_state(False)
                filter_widget.set_options_state(False, SliderType.GENERAL)
                filter_widget.control_box.setEnabled(False)
            self.filter_merger.set_options_state(False)
            self.filter_merger.control_box.setEnabled(False)
            self.gradient_filter.set_options_state(False, SliderType.GENERAL)

    def refresh(self):
        for filter_widget in self.filter_widgets:
            filter_widget.update_filtration()
            filter_widget.refresh(self.current_position)
        raw_mask = self._get_raw_mask()
        self.filter_merger.update_filtered_data(raw_mask)
        self.gradient_filter.update_filtration()
        self.gradient_filter.refresh()

    def _get_raw_mask(self):
        raw_filter_mask = 1
        for filter_widget in self.filter_widgets:
            raw_filter_mask *= filter_widget.get_filter_mask()
        return raw_filter_mask

    def _sep_filter_params_updated(self):
        LOGGER.info('Sep filter parameters released')
        raw_filter_mask = self._get_raw_mask()
        self.filter_merger.update_filtered_data(raw_filter_mask)
        #self.result_mask_update()

    def result_mask_updated(self):
        if self.parent():
            self.parent().result_mask_update(self.filter_merger.get_result_mask().astype(bool).astype(np.uint8))

    def set_original_data(self, original_data):
        for filter_widget in self.filter_widgets:
            filter_widget.set_original_data(original_data)
        self.gradient_filter.set_original_data(original_data)

    def set_options_state(self, state, slider_type=None):
        for filter_widget in self.filter_widgets:
            filter_widget.set_options_state(state, slider_type)

    def get_result_mask(self):
        result_mask = 1
        for filter_widget in self.filter_widgets:
            result_mask *= filter_widget.get_filter_mask()
        return result_mask

    def clean_all(self):
        for filter_widget in self.filter_widgets:
            filter_widget.clear_data()
        self.gradient_filter.clear_data()
        self.filter_merger.clear_all()
        self.current_position = 0


# app = QApplication([])
# widget = FilterControlWidget()
# widget.setWindowTitle('FilterControlWidget')
# widget.show()
# app.exec_()
