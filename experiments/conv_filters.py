# %%

import numpy as np
import numpy.linalg as LA
from scipy.ndimage import generic_gradient_magnitude, sobel
from skimage.morphology import closing, square, remove_small_holes
from skimage.segmentation import clear_border
from skimage.filters import threshold_otsu
from spine_detector.utils import load_mri_research, timer
import matplotlib.pyplot as plt

print(f'Read data')

first_patient_path = '../test-data/0001'
T1_research = f'{first_patient_path}/T1_TSE_SAG_320_0003'

pixel_arrays, raw_dicoms = load_mri_research(T1_research)

print(f'Stack data')
original_data = np.zeros((15, 320, 320))

for i, array in enumerate(pixel_arrays):
    original_data[i] = array


@timer
def get_dispersion_mask(data, rect_shape=(3, 3, 3)):
    dispersion_matrix = np.zeros_like(data)
    for x in range(1, data.shape[0] - 1):
        for y in range(1, data.shape[1] - 1):
            for z in range(1, data.shape[2] - 1):
                dispersion_matrix[x][y][z] = np.var(data[x - 1:x + 1, y - 1:y + 1, z - 1:z + 1])
    return dispersion_matrix


@timer
def get_gradient_mask(data):
    gradient_matrix = np.zeros_like(data)
    for x in range(1, data.shape[0] - 1):
        for y in range(1, data.shape[1] - 1):
            for z in range(1, data.shape[2] - 1):
                gradient_matrix[x][y][z] = LA.norm(np.gradient(data[x - 1:x + 1, y - 1:y + 1, z - 1:z + 1]))
    return gradient_matrix


@timer
def otsu_filtering(original_pixel_array):
    """
    Данный делает следующее:
        * высчитывает трешхолд по отцу
        * накладывает маску
        * возращает pixel_array отрабатанный по маске
    :param original_pixel_array:
    :return:
    """
    thresh = threshold_otsu(original_pixel_array)
    bw = closing(original_pixel_array > thresh, square(1))

    # set borders with 5
    bw[:, 5] = True
    bw[:, 315] = True
    bw[5, :] = True
    bw[315, :] = True

    cleared = clear_border(bw)

    cleared_pixel_array = original_pixel_array * cleared

    return cleared_pixel_array


@timer
def filter_with_dispersion(original_data, dispersion_matrix, slice_number, k=1):
    max_dispersion = np.max(dispersion_matrix)
    print(f'Max dispersion: {max_dispersion}')
    coef = max_dispersion * k / 255
    print(f'coef {coef}')

    tst_disp = np.invert(dispersion_matrix[slice_number] > coef)
    tst_image = original_data[slice_number]

    after_otsu = otsu_filtering(tst_image)
    after_dispersion = after_otsu * tst_disp
    fig, ax = plt.subplots(ncols=2, nrows=2, figsize=(12, 12))

    ax[0][0].set_title('Original image')
    ax[0][0].imshow(tst_image, cmap='gray')
    ax[0][0].axis('off')

    ax[0][1].set_title('After otsu')
    ax[0][1].imshow(after_otsu, cmap='gray')
    ax[0][1].axis('off')

    ax[1][0].set_title('Dispersion map')
    ax[1][0].imshow(tst_disp, cmap='gray')
    ax[1][0].axis('off')

    ax[1][1].set_title('After all')
    ax[1][1].imshow(after_dispersion, cmap='gray')
    ax[1][1].axis('off')

    plt.show()


# filter_with_dispersion(original_data, get_dispersion_mask(original_data), 6)

@timer
def filter_with_gradient(original_data, gradient_matrix, slice_number, k=20):
    max_gradient = np.max(gradient_matrix)
    coef = max_gradient * k / 255
    tst_gradient = gradient_matrix[slice_number] > coef
    tst_image = original_data[slice_number]

    after_otsu = otsu_filtering(tst_image)
    after_dispersion = after_otsu * tst_gradient
    fig, ax = plt.subplots(ncols=2, nrows=2, figsize=(12, 12))

    ax[0][0].set_title('Original image')
    ax[0][0].imshow(tst_image, cmap='gray')
    ax[0][0].axis('off')

    ax[0][1].set_title('After otsu')
    ax[0][1].imshow(after_otsu, cmap='gray')
    ax[0][1].axis('off')

    ax[1][0].set_title('Gradient map')
    ax[1][0].imshow(tst_gradient, cmap='gray')
    ax[1][0].axis('off')

    ax[1][1].set_title('After all')
    ax[1][1].imshow(after_dispersion, cmap='gray')
    ax[1][1].axis('off')

    plt.show()


# filter_with_gradient(original_data, gradient_matrix=get_gradient_mask(original_data), slice_number=6)

@timer
def filter_by_something(original_data, generic_mask, mask_type, slice_number, k=20):
    max_mask_val = np.max(generic_mask)
    coef = max_mask_val * k / 255

    tst_masked_image = np.invert(generic_mask[slice_number] > coef)
    tst_image = original_data[slice_number]

    after_otsu = otsu_filtering(tst_image)

    after_masking = after_otsu * tst_masked_image
    # after_dispersion = tst_image * tst_masked_image
    fig, ax = plt.subplots(ncols=2, nrows=2, figsize=(12, 12))

    ax[0][0].set_title('Original image')
    ax[0][0].imshow(tst_image, cmap='gray')
    ax[0][0].axis('off')

    ax[0][1].set_title('After otsu')
    ax[0][1].imshow(after_otsu, cmap='gray')
    ax[0][1].axis('off')

    ax[1][0].set_title(f'Mask {mask_type} map')
    ax[1][0].imshow(tst_masked_image, cmap='gray')
    ax[1][0].axis('off')

    ax[1][1].set_title('After all')
    ax[1][1].imshow(after_masking, cmap='gray')
    ax[1][1].axis('off')

    plt.show()


mag = generic_gradient_magnitude(original_data, sobel)


# filter_by_something(original_data, mag, 'auto_sobel_3d', 7, 45)

@timer
def filter_by_mask(data, mask, k_min=-1, k_max=255):
    mask_max = np.max(mask)
    coef_max = k_max * mask_max / 255
    coef_min = k_min * mask_max / 255

    filtered_mask = np.invert((mask <= coef_max) & (mask >= coef_min))

    masked_data = data * filtered_mask

    return masked_data


def prepare_for_view(raw_array: np.array, position: int, threshold: int = 10, k: int = 5) -> np.array:
    """

    :param raw_array: входной снимко
    :param position: позиция снимка в последовательности
    :param threshold: порог для отображения пикселя
    :param k: коэффициент для корректного отображения позции на снимке
    :return: (100, 320, 320, 4) массов со снимком
    """
    new = np.zeros((100, 320, 320, 4), dtype=np.uint8)
    for i in range(raw_array.shape[0]):
        for j in range(raw_array.shape[1]):
            val = raw_array[i][j]
            if val > threshold:
                new[position * k][j][raw_array.shape[0] - i - 1] = np.array([val, val, val, 255])
    return new

from skimage.morphology import remove_small_objects

k_min = 20
k_max = 60

# sobel_filtered = filter_by_mask(original_data, mag, k_min=k_min, k_max=k_max)
# dispersion_filtered = filter_by_mask(original_data, get_dispersion_mask(original_data), k_min=k_min, k_max=k_max)

filtered_data = np.zeros((100, 320, 320, 4))

for i in range(len(original_data)):
    after_otsu = otsu_filtering(original_data[i])
    fix_small_holes = remove_small_holes(after_otsu > 0)
    delete_small_objects = remove_small_objects(fix_small_holes, min_size=500)

    filtered_data += prepare_for_view(original_data[i] * delete_small_objects, position=i)

np.save('../results/otsu_and_small_holes_object_remove', filtered_data)

