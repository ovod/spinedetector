from PyQt5 import QtWidgets
import numpy as np

from spine_detector.constants import LOGGER
from spine_detector.core_logic import FilterMergerCore
from spine_detector.domain_classes import SliderType
from spine_detector.widgets.common.custom_image_view import CustomImageView
from spine_detector.widgets.common.parameters_box_widget import ParameterSliderBoxWidget


class FilterMergerWidget(QtWidgets.QWidget):

    def setup_ui(self, options_spec):
        self.setFixedSize(400, 800)
        central_layout = QtWidgets.QVBoxLayout(self)

        label = QtWidgets.QLabel('Result mask')
        central_layout.addWidget(label)

        self.control_box = QtWidgets.QGroupBox(self)
        control_box_layout = QtWidgets.QHBoxLayout()
        self.control_box.setLayout(control_box_layout)

        self.advanced_rbt = QtWidgets.QRadioButton(self)
        self.advanced_rbt.setText('Advanced options')
        self.advanced_rbt.setChecked(False)
        self.advanced_rbt.setAutoExclusive(False)

        control_box_layout.addWidget(self.advanced_rbt)

        self.reset_btn = QtWidgets.QPushButton(self)
        self.reset_btn.setText('Reset to defaults')

        control_box_layout.addWidget(self.reset_btn)

        self.control_box.setEnabled(False)
        central_layout.addWidget(self.control_box)

        self.control_image = CustomImageView(self)
        central_layout.addWidget(self.control_image)

        self.parameters_box = ParameterSliderBoxWidget(self, options_spec)
        central_layout.addWidget(self.parameters_box)

        central_layout.addItem(
            QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        )

    def __init__(self, parent=None):
        super(FilterMergerWidget, self).__init__(parent=parent)
        self.core = FilterMergerCore()
        self.setup_ui(self.core.get_specification())
        self.parameters_box.set_sliders_enabled(False)

        self.current_position = 0
        self.parameters_box.set_parameter_handler(self.parameters_update)
        self.advanced_rbt.clicked.connect(self.advanced_parameters_set_activate)
        self.control_widget = None

    def set_controller(self, control_widget):
        self.control_widget = control_widget

    def advanced_parameters_set_activate(self):
        if self.advanced_rbt.isChecked():
            self.set_options_state(True, SliderType.ADVANCED)
        else:
            self.set_options_state(False, SliderType.ADVANCED)

    def slice_update(self):
        image = self.core.get_filtered_image(self.current_position)
        if type(image) is np.ndarray:
            self.control_image.set_image(image)

    def get_result_mask(self):
        return self.core.get_filtered_mask()

    def update_filtered_data(self, raw_filtered_data):
        self.set_options_state(True)
        LOGGER.info('Raw filtered_data updated')
        self.core.set_original_data(raw_filtered_data)
        self._update_calculation()
        self.control_image.set_image(self.core.get_filtered_image(self.current_position))
        if self.control_widget:
            self.control_widget.result_mask_updated()

    def parameters_update(self):
        LOGGER.info('Parameters updated')
        self._update_calculation()
        self.control_image.set_image(self.core.get_filtered_image(self.current_position))
        if self.control_widget:
            self.control_widget.result_mask_updated()

    def _update_calculation(self):
        current_values = self.parameters_box.get_current_values()
        self.core.update_filtration(current_values)

    def clear_all(self):
        self.set_options_state(False)
        self.core.clean_data()
        self.control_image.set_image(np.zeros((320, 320)))
        self.current_position = 0

    def set_options_state(self, state: bool, slider_type=SliderType.GENERAL):
        self.parameters_box.set_sliders_enabled(state, slider_type)

# app = QApplication([])
# widget = FilterMergerWidget()
# widget.show()
# app.exec_()
