from abc import ABC, abstractmethod
from typing import Dict

import numpy as np
from matplotlib import pyplot
from skimage.morphology import remove_small_objects, remove_small_holes
from skimage.segmentation import clear_border
from scipy.ndimage import binary_dilation, binary_erosion

from skimage.filters import threshold_local

from spine_detector.constants import LOGGER
from spine_detector.domain_classes import FilterTypes, SliderType
from spine_detector.masking.gradient import get_gradient, box_filtering_via_gauss


class AbstractFilter(ABC):
    filter_params: Dict
    original_data: np.ndarray
    filtered_data: np.ndarray
    name: str = 'No name filter'

    def __init__(self, filter_params=None):
        if filter_params is None:
            filter_params = dict()
        self.filter_params: dict = filter_params

    def set_original_data(self, original_data: np.ndarray):
        self.original_data = original_data
        self.filtered_data = np.zeros_like(original_data, dtype=np.bool)

    def get_filtered_image(self, position: int = 0) -> np.ndarray:
        if type(self.filtered_data) is np.ndarray:
            return self.filtered_data[position]
        return np.zeros((320, 320))

    def get_filtered_mask(self):
        return self.filtered_data

    def clean_data(self):
        self.original_data = None
        self.filtered_data = None
        self.filter_params = dict()

    @abstractmethod
    def update_filtration(self, new_filtration_params):
        pass


class EdgeFilter(AbstractFilter):
    name = 'Edge filter'

    def update_filtration(self, new_filtration_params):
        self.filter_params.update(new_filtration_params)
        LOGGER.info(f'Run filtration with new params {self.filter_params}')
        alpha = self.filter_params.get('alpha', 0.2)
        k_min = self.filter_params.get('k_min', 38.)
        gamma = self.filter_params.get('gamma', .75)
        obj_size = self.filter_params.get('obj_size', 512)
        hole_size = self.filter_params.get('hole_size', 256)
        self._normalized_data = np.power(self.original_data / 255, gamma) * 255.

        self.filtered_data = get_gradient(self._normalized_data, alpha=alpha) < k_min

        for i in range(self.filtered_data.shape[0]):
            self.filtered_data[i] = remove_small_objects(self.filtered_data[i], obj_size)
            self.filtered_data[i] = remove_small_holes(self.filtered_data[i], area_threshold=hole_size)

    @staticmethod
    def get_specification():
        return dict(
            gamma=dict(label='1/Gamma', value_type='float', init_value=0.75, min_value=.01, max_value=2.),
            k_min=dict(label='K_max', value_type='float', init_value=35., min_value=.01, max_value=50.),
            alpha=dict(label='Alpha', value_type='float', init_value=0.3, min_value=.01, max_value=1.),
            obj_size=dict(label='Obj size', min_value=1, max_value=1024, slider_type=SliderType.ADVANCED),
            hole_size=dict(label='Hole size', min_value=1, max_value=1024, init_value=25, slider_type=SliderType.ADVANCED),
        )


class GradientFilter(AbstractFilter):
    def update_filtration(self, new_filtration_params):
        self.filter_params.update(new_filtration_params)
        alpha = self.filter_params.get('alpha', 0.2)
        upper_edge = self.filter_params.get('upper_edge')

        gradient_data = get_gradient(self.original_data, alpha=alpha)
        gradient_data[gradient_data > upper_edge] = float(upper_edge)
        jet_cmap = pyplot.get_cmap('jet')
        gradient_data = (gradient_data - gradient_data.min()) / gradient_data.max()
        self.filtered_data = jet_cmap(gradient_data) * 255

    @staticmethod
    def get_specification():
        return dict(
            alpha=dict(label='Alpha', value_type='float', min_value=0.01, init_value=0.2, max_value=1.),
            upper_edge=dict(label='upper_edge', min_value=1, init_value=20, max_value=256)
        )


class GaussBasedFilter(AbstractFilter):
    name = 'Area filter'

    def set_original_data(self, original_data):
        self.original_data = original_data
        self._normalized_data = np.power(original_data / 256, 0.75) * 256.

    def update_filtration(self, parameters):
        self.filter_params.update(parameters)
        LOGGER.info(f'Run filtration with new params {self.filter_params}')
        alpha = self.filter_params['alpha']
        x = self.filter_params['x']
        y = self.filter_params['y']
        z = self.filter_params['z']
        k_min = self.filter_params['k_min']
        degree = self.filter_params['grad_degree']
        inflation_coef = self.filter_params.get('infl_k', .2)
        size = (x, y, z)

        self.mask_data = box_filtering_via_gauss(get_gradient(self._normalized_data, alpha=alpha),
                                                 degree=degree,
                                                 size=size)

        self.filtered_data = (np.zeros_like(self.original_data) + self.inflate(self.mask_data < k_min, size,
                                                                               threshold=inflation_coef))

        self.filtered_data = self.filtered_data.astype(np.uint8)

    def inflate(self, mask, radius, threshold=1):
        mask = mask.astype(np.float)
        mask = box_filtering_via_gauss(mask, degree=1, size=radius) + 1e-100
        radius = np.array(radius)
        mask = mask * np.power(np.prod(radius), 1 / 3) * np.power(2 * np.pi, 1 / 2) * 4
        LOGGER.debug(f'Arg before log: {mask.max()}')
        mask = np.log(mask)
        mask[mask > 0] = 0
        mask = np.sqrt(-mask * 2)
        return mask <= threshold

    @staticmethod
    def get_specification():
        return dict(
            infl_k=dict(label='Inlf_k', value_type='float', init_value=0.9, min_value=.01, max_value=30.),
            k_min=dict(label='K_max', value_type='float', init_value=18.8, min_value=.01, max_value=40.),
            alpha=dict(label='Alpha', value_type='float', init_value=0.2, min_value=.01, max_value=1.),
            grad_degree=dict(label='Grad. Degree', init_value=0.9, value_type='log', min_value=.0047, max_value=8., slider_type=SliderType.ADVANCED),
            x=dict(label='X', value_type='float', init_value=1.2, min_value=.01, max_value=15., slider_type=SliderType.ADVANCED),
            y=dict(label='Y', value_type='float', init_value=1.8, min_value=.01, max_value=15., slider_type=SliderType.ADVANCED),
            z=dict(label='Z', value_type='float', init_value=2.7, min_value=.01, max_value=15., slider_type=SliderType.ADVANCED),
        )


class ThresholdFilterUnit(AbstractFilter):

    def set_original_data(self, original_data):
        self.original_data = original_data
        self.filtered_data = np.zeros_like(original_data)

    def update_filtration(self, parameters):
        self.filter_params.update(parameters)
        print(self.filter_params)
        bright_min = self.filter_params.get('bright_min', 40)
        bright_max = self.filter_params.get('bright_max', 150)
        self.filtered_data = (self.original_data > bright_min) * (self.original_data < bright_max)

    @staticmethod
    def get_specification():
        return dict(
            bright_min=dict(label='Bright min', value_type='int', min_value=0, max_value=255, init_value=40),
            bright_max=dict(label='Bright max', value_type='int', min_value=0, max_value=255, init_value=150)
        )


class AdaptiveThresholdFilter(AbstractFilter):
    name = 'Adaptive Intentsity + Threshold filters'

    def update_filtration(self, new_filtration_params):
        self.filter_params.update(new_filtration_params)
        LOGGER.info(f'{self.filter_params}')
        gamma = self.filter_params.get('gamma', 0.75)
        edge_min = self.filter_params.get('edge_min', 0.04)
        abs_min = self.filter_params.get('abs_min', 0.04)
        edge_max = self.filter_params.get('edge_max', 1.)
        x = self.filter_params.get('x', 3)
        y = self.filter_params.get('y', 3)
        z = self.filter_params.get('z', 5)
        hole_size = self.filter_params.get('hole_size', 3)

        normalized_data = np.power(self.original_data / 255, gamma)
        gauss_norm_data = box_filtering_via_gauss(normalized_data, degree=1, size=(x, y, z))
        diff_data = normalized_data - gauss_norm_data
        diff_data -= diff_data.min()
        self.filtered_data = np.array((diff_data < edge_max) * (diff_data > edge_min) * (normalized_data > abs_min),
                                      dtype=np.uint8)
        LOGGER.info(f'{diff_data.min()}  {diff_data.max()}')
        for i in range(self.filtered_data.shape[0]):
            self.filtered_data[i] = remove_small_holes(self.filtered_data[i], area_threshold=hole_size)
        self.filtered_data *= 255

    @staticmethod
    def get_specification():
        return dict(
            edge_min=dict(label='Edge min', value_type='float', min_value=0., max_value=2., init_value=0.48),
            gamma=dict(label='1/Gamma', value_type='float', min_value=.01, max_value=2., init_value=0.75, slider_type=SliderType.ADVANCED),
            abs_min=dict(label='Absolute min', value_type='float', min_value=0., max_value=1., init_value=0.2, slider_type=SliderType.ADVANCED),
            hole_size=dict(label='Hole size', min_value=0, max_value=1024, init_value=150, slider_type=SliderType.ADVANCED),
            x=dict(label='X', min_value=1, max_value=15, init_value=3, slider_type=SliderType.ADVANCED),
            y=dict(label='Y', min_value=1, max_value=40, init_value=5, slider_type=SliderType.ADVANCED),
            z=dict(label='Z', min_value=1, max_value=40, init_value=5, slider_type=SliderType.ADVANCED),
        )


class LocalThresholdCoreFilter(AbstractFilter):
    method_map = {
        0: 'gaussian',
        1: 'mean',
        2: 'median'
    }

    def update_filtration(self, new_filtration_params):
        self.filter_params.update(new_filtration_params)
        block_size = self.filter_params.get('block_size', 3)
        method = self.method_map[self.filter_params.get('method', 0)]

        for i in range(self.original_data.shape[0]):
            self.filtered_data[i] = self.original_data[i] > threshold_local(self.original_data[i],
                                                                            block_size,
                                                                            method)

    @staticmethod
    def get_specification():
        return dict(
            block_size=dict(label='Block size', min_value=3, max_value=151),
            method=dict(label='Method', min_value=0, max_value=2)
        )


class FilterMergerCore(AbstractFilter):
    def update_filtration(self, new_filtration_params):
        self.filter_params.update(new_filtration_params)
        erosion_iter = self.filter_params.get('erosion_iter', 0)
        dilation_iter = self.filter_params.get('dilation_iter', 0)
        hole_size = self.filter_params.get('hole_size', 0)
        obj_size = self.filter_params.get('obj_size', 0)
        border_size = self.filter_params.get('border_size', 5)

        tmp_data = self.original_data.copy().astype(np.bool)

        if border_size:
            border_mask = np.ones(self.original_data.shape[-2:], dtype=np.bool)
            border_mask[border_size:-border_size, border_size:-border_size] = False
            for i in range(self.original_data.shape[0]):
                tmp_data[i] = clear_border(tmp_data[i] + border_mask)

        for i in range(self.original_data.shape[0]):
            if erosion_iter:
                tmp_data[i] = binary_erosion(tmp_data[i], iterations=erosion_iter)
            tmp_data[i] = remove_small_holes(tmp_data[i], area_threshold=hole_size)
            tmp_data[i] = remove_small_objects(tmp_data[i], min_size=obj_size)
            if dilation_iter:
                tmp_data[i] = binary_dilation(tmp_data[i], iterations=dilation_iter)

        #tmp_data = tmp_data * self.original_data
        self.filtered_data = tmp_data.astype(np.uint8) * 255

    @staticmethod
    def get_specification():
        return dict(
            erosion_iter=dict(label='Erosion iter', min_value=0, max_value=30, init_value=1),
            dilation_iter=dict(label='Dilation iter', min_value=0, max_value=30, init_value=3),
            hole_size=dict(label='Hole size', min_value=0, max_value=512, slider_type=SliderType.ADVANCED),
            obj_size=dict(label='Obj size', min_value=0, max_value=512, init_value=320, slider_type=SliderType.ADVANCED),
            border_size=dict(label='Border size', min_value=0, max_value=100, init_value=5, slider_type=SliderType.ADVANCED)
        )


FILTER_MAP = {
    FilterTypes.GAUSS_BASED: GaussBasedFilter,
    FilterTypes.EDGE_FILTER: EdgeFilter,
    FilterTypes.THRESHOLD_FILTER: ThresholdFilterUnit,
    FilterTypes.ADAPTIVE_THRESHOLD_FILTER: AdaptiveThresholdFilter,
    FilterTypes.GRADIENT_FILTER: GradientFilter,
    FilterTypes.LOCAL_THRESHOLD_FILTER: LocalThresholdCoreFilter
}
