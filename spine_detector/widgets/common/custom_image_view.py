from typing import Callable

import pyqtgraph as pg
import numpy as np
from PyQt5 import QtCore, QtWidgets


class CustomViewBox(pg.ViewBox):
    def __init__(self, image_item: pg.ImageItem, **kwargs):
        super().__init__(**kwargs)
        self.invertY()
        self.image_item = image_item
        self.addItem(self.image_item)
        self.parent_handler = None
        self.custom_behavior = True
        self.setBackgroundColor((127, 127, 127))

    # def mouseClickEvent(self, ev):
    #     if ev.button() == QtCore.Qt.RightButton:
    #         if self.custom_behavior:
    #             items = self.mapSceneToView(ev.scenePos())
    #             if self.parent_handler:
    #                 self.parent_handler(items)
    #         else:
    #             ev.accept()
    #             self.raiseContextMenu(ev)
    #
    def set_parent_handler(self, handler: Callable):
        pass
#        self.parent_handler = handler
    #
    # def mouseDragEvent(self, ev, axis=None):
    #     ## if axis is specified, event will only affect that axis.
    #     ev.accept()  ## we accept all buttons
    #
    #     pos = ev.pos()
    #     lastPos = ev.lastPos()
    #     dif = pos - lastPos
    #     dif = dif * -1
    #
    #     ## Ignore axes if mouse is disabled
    #     mouseEnabled = np.array(self.state['mouseEnabled'], dtype=np.float)
    #     mask = mouseEnabled.copy()
    #     if axis is not None:
    #         mask[1 - axis] = 0.0
    #
    #     ## Scale or translate based on mouse button
    #     if ev.button() & (QtCore.Qt.LeftButton | QtCore.Qt.MidButton):
    #         if self.state['mouseMode'] == pg.ViewBox.RectMode:
    #             if ev.isFinish():  ## This is the final move in the drag; change the view scale now
    #                 # print "finish"
    #                 self.rbScaleBox.hide()
    #                 # ax = QtCore.QRectF(Point(self.pressPos), Point(self.mousePos))
    #                 ax = QtCore.QRectF(pg.Point(ev.buttonDownPos(ev.button())), pg.Point(pos))
    #                 ax = self.childGroup.mapRectFromParent(ax)
    #                 self.showAxRect(ax)
    #                 self.axHistoryPointer += 1
    #                 self.axHistory = self.axHistory[:self.axHistoryPointer] + [ax]
    #             else:
    #                 ## update shape of scale box
    #                 self.updateScaleBox(ev.buttonDownPos(), ev.pos())
    #         else:
    #             tr = dif * mask
    #             tr = self.mapToView(tr) - self.mapToView(pg.Point(0, 0))
    #             x = tr.x() if mask[0] == 1 else None
    #             y = tr.y() if mask[1] == 1 else None
    #
    #             self._resetTarget()
    #             if x is not None or y is not None:
    #                 self.translateBy(x=x, y=y)
    #             self.sigRangeChangedManually.emit(self.state['mouseEnabled'])


class CustomViewWidget(pg.GraphicsView):
    def __init__(self, parent, width=380, height=380):
        super().__init__(parent=parent)
        self.image_item = pg.ImageItem()
        self.view_box = CustomViewBox(self.image_item)

        self.setCentralItem(self.view_box)

        self.setFixedSize(width, height)

    def custom_behavior_enabled(self, flag):
        self.view_box.custom_behavior = flag

    def set_image(self, data: np.ndarray):
        if data.dtype == bool:
            data = data.astype(np.uint8)
        self.image_item.setImage(data)


class UI_CustomImageView(object):
    def setupUI(self, custom_image_view, width=380, height=380):
        custom_image_view.resize(width, height)
        custom_image_view.setMinimumSize(QtCore.QSize(width, height))
        custom_image_view.setMaximumSize(QtCore.QSize(width, height))

        self.central_layout = QtWidgets.QVBoxLayout(custom_image_view)

        self.image_view = CustomViewWidget(custom_image_view)
        self.central_layout.addWidget(self.image_view)

        # self.custom_behavior_rbtn = QtWidgets.QRadioButton()
        # self.custom_behavior_rbtn.setText('Drag')
        # self.custom_behavior_rbtn.setChecked(True)
        # self.central_layout.addWidget(self.custom_behavior_rbtn)


class CustomImageView(QtWidgets.QWidget):

    def setup_ui(self, width, height):
        self.setFixedSize(width, height)

        central_layout = QtWidgets.QVBoxLayout(self)

        self.image_view = CustomViewWidget(self)
        central_layout.addWidget(self.image_view)

    def __init__(self, parent, with_custom_behavior=False, width=380, height=380, name=None, connect_to='main_image'):
        super(CustomImageView, self).__init__(parent=parent)
        self.setup_ui(width, height)

        if name:
            self.image_view.view_box.register(name)

        if connect_to:
            self.image_view.view_box.setYLink(connect_to)
            self.image_view.view_box.setXLink(connect_to)

        # state
        self.custom_behavior = False
        # connectors
        # if with_custom_behavior:
        #     self.custom_behavior_rbtn.clicked.connect(self.change_dragable_state)
        # else:
        #     self.custom_behavior_rbtn.hide()

        # reinit mouse click event

    def set_image(self, image_data):
        self.image_view.set_image(image_data)

    def set_behavior_handler(self, handler: Callable):
        self.image_view.view_box.set_parent_handler(handler)

    # def change_dragable_state(self):
    #     if self.custom_behavior_rbtn.isChecked():
    #         print(True)
    #         self.image_view.custom_behavior_enabled(True)
    #     else:
    #         print(False)
    #         self.image_view.custom_behavior_enabled(False)
