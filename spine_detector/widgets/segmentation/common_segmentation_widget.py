import numpy as np
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt

from spine_detector.constants import LOGGER
from spine_detector.domain_classes import ComponentInfo
from spine_detector.event_logic import Component
from spine_detector.widgets.segmentation.segmentation_3d_widget import SegmentationScene3dWidget
from spine_detector.widgets.segmentation.segmentation_image_widget import SegmentationImageWidget
from spine_detector.widgets.segmentation.segmentation_info_widget import SegmentationInfoWidget


class UI_SegmentationCommonWidget(object):
    def setupUi(self, segmentation_common_widget):
        segmentation_common_widget.resize(1200, 800)
        segmentation_common_widget.setMinimumSize(QtCore.QSize(1200, 800))
        segmentation_common_widget.setMaximumSize(QtCore.QSize(1200, 800))

        self.central_layout = QtWidgets.QHBoxLayout(segmentation_common_widget)

        self.segmentation_2d_view = SegmentationImageWidget(segmentation_common_widget)
        self.central_layout.addWidget(self.segmentation_2d_view)

        self.segmentation_3d_view = SegmentationScene3dWidget(segmentation_common_widget)
        self.central_layout.addWidget(self.segmentation_3d_view)

        self.segmentation_info = SegmentationInfoWidget(segmentation_common_widget)
        self.central_layout.addWidget(self.segmentation_info)


class SegmentationCommonWidget(QtWidgets.QWidget, UI_SegmentationCommonWidget, Component):
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.setupUi(self)

        self.segmentation_2d_view.segmentation_params.set_parameter_handler(self.segmentation_params_updated)
        self.current_position = 0

        self.segmentation_info.components_list.item_select_action = self.component_selected_handler
        self.segmentation_info.components_list.item_unselect_action = self.component_unselected_handler

    def set_original_data(self, original_data):
        self.original_data = original_data

    def set_source_data(self, source_data):
        self.segmentation_2d_view.set_source_data(source_data)

    def update_slice(self, position):
        self.current_position = position
        self.segmentation_2d_view.refresh(position, False)

    def refresh(self, position=None, colored_enabled=False):
        if position is None:
            position = self.current_position

        self._refresh(position)

    def segmentation_params_updated(self):
        self._refresh(self.current_position)
        self.parent().notify()

    def _refresh(self, position):
        self.segmentation_2d_view.update_segmentation()
        self.segmentation_2d_view.refresh(position)

        components = self.segmentation_2d_view.get_components_3d()
        self.segmentation_3d_view.remove_data()
        self.segmentation_3d_view.update_components(components, k=5)

        component_infos = [ComponentInfo.from_region(region) for region in
                           components]
        self.segmentation_info.delete_all_items()
        self.segmentation_info.add_items(*component_infos)

    def get_regions_3d(self):
        return self.segmentation_2d_view.get_components_3d()

    def clean_all(self):
        self.original_data = None
        self.segmentation_2d_view.clean_all()
        self.segmentation_3d_view.remove_data()
        self.segmentation_info.delete_all_items()

    # change handlers for info items

    def component_selected_handler(self, item):
        LOGGER.debug(f'Component with label {item.text()} selected. Change segmentation position')
        item: ComponentInfo = item.data(Qt.UserRole)
        center = tuple(item.region.coords.mean(axis=0).astype(np.uint8))
        self.segmentation_3d_view.change_scene_center(center)

    def component_unselected_handler(self, item):
        LOGGER.debug(f'Component with label {item.text()} unselected')

        self.segmentation_3d_view.reset_center_to_default()

#
# app = QApplication(sys.argv)
# widget = SegmentationCommonWidget(None)
# widget.show()
# app.exec_()
