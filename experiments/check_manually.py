import numpy as np
from skimage.morphology import remove_small_objects, remove_small_holes

from spine_detector.filters.otsu import otsu_with_borders
from spine_detector.utils import load_mri_research, prepare_for_view

base_result_path = '../results'
first_patient_path = '../test-data/0001'
T1_research = f'{first_patient_path}/T1_TSE_SAG_320_0003'

pixel_arrays, _ = load_mri_research(T1_research)

print(f'Stack data')
original_data = np.zeros((15, 320, 320))

for i, array in enumerate(pixel_arrays):
    original_data[i] = array

filtered_data = np.zeros((100, 320, 320, 4))

for i in range(len(original_data)):
    after_otsu = otsu_with_borders(original_data[i])
    fix_small_holes = remove_small_holes(after_otsu > 0)
    delete_small_objects = remove_small_objects(fix_small_holes, min_size=600)

    filtered_data += prepare_for_view(original_data[i] * delete_small_objects, position=i)

np.savez_compressed(f'{base_result_path}/otsu_and_small_holes_object_remove_min_size_600', data=filtered_data)