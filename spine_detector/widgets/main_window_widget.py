from pydicom.errors import InvalidDicomError

from spine_detector.constants import LOGGER
import os
from datetime import datetime

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QFileDialog
import numpy as np

from spine_detector.event_logic import Component, OriginalDataLoaded, ClearAllEvent, TabChangedEvent
from spine_detector.utils import load_mri, rescale_original_image
from spine_detector.widgets.main_view_widget import MainViewWidget


class Ui_MainWindow(object):
    def setupUi(self, main_window):
        main_window.setObjectName("MainWindow")
        main_window.setEnabled(True)
        main_window.resize(1280, 800)
        main_window.setMinimumSize(QtCore.QSize(1700, 900))
        main_window.setMaximumSize(QtCore.QSize(1700, 900))
        self.central_widget = QtWidgets.QWidget(main_window)
        self.central_layout = QtWidgets.QHBoxLayout(self.central_widget)

        self.main_view_widget = MainViewWidget(self.central_widget)
        self.central_layout.addWidget(self.main_view_widget)

        self.central_layout.addWidget(self.central_widget)

        self.tabWidget = QtWidgets.QTabWidget(self.central_widget)
        self.tabWidget.setFixedSize(1280, 900)
        self.central_layout.addWidget(self.tabWidget)
        main_window.setCentralWidget(self.central_widget)

        self.menubar = QtWidgets.QMenuBar(main_window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1280, 21))
        self.menubar.setNativeMenuBar(True)
        self.menubar.setObjectName("menubar")
        self.menu_open_mri_dir = QtWidgets.QMenu(self.menubar)
        self.menu_open_mri_dir.setObjectName("menu_open_mri_dir")
        main_window.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(main_window)
        self.statusbar.setObjectName("statusbar")
        main_window.setStatusBar(self.statusbar)
        self.actionOpen_mri_dir = QtWidgets.QAction(main_window)
        self.actionOpen_mri_dir.setObjectName("actionOpen_mri_dir")
        self.actionClose = QtWidgets.QAction(main_window)
        self.actionClose.setObjectName("actionClose")
        self.action_open_npz_file = QtWidgets.QAction(main_window)
        self.action_open_npz_file.setObjectName("action_open_npz_file")
        self.action_save_npz_file = QtWidgets.QAction(main_window)
        self.action_save_npz_file.setObjectName("action_save_npz_file")
        self.menu_open_mri_dir.addAction(self.actionOpen_mri_dir)
        self.menu_open_mri_dir.addAction(self.action_open_npz_file)
        self.menu_open_mri_dir.addAction(self.action_save_npz_file)
        self.menu_open_mri_dir.addAction(self.actionClose)
        self.menubar.addAction(self.menu_open_mri_dir.menuAction())

        self.retranslateUi(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.menu_open_mri_dir.setTitle(_translate("MainWindow", "File"))
        self.actionOpen_mri_dir.setText(_translate("MainWindow", "Open mri dir"))
        self.actionClose.setText(_translate("MainWindow", "Close"))
        self.action_open_npz_file.setText(_translate("MainWindow", "Open NPZ file"))
        self.action_save_npz_file.setText(_translate("MainWindow", "Save NPZ file"))


class MainWindowWidget(QtWidgets.QMainWindow, Ui_MainWindow, Component):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.set_listeners([self.main_view_widget])
        self.original_data: np.ndarray = None

        # connectors
        self.tabWidget.currentChanged.connect(self.tab_changed)
        # menu bar actions
        self.actionOpen_mri_dir.triggered.connect(self.open_mri_dir)
        self.action_open_npz_file.triggered.connect(self.open_npz_file)
        self.action_save_npz_file.triggered.connect(self.save_npz_file)
        self.actionClose.triggered.connect(self.close_research)

    def add_tab_widget(self, tab_widget: Component, tab_name: str, tab_object_name=None):
        self.tabWidget.addTab(tab_widget, tab_name)
        tab_widget.set_listeners(self._listeners)
        tab_widget.add_listener(self)
        for listener in self._listeners:
            listener.add_listener(tab_widget)

        self.add_listener(tab_widget)
        self.__setattr__(tab_object_name, tab_widget)

    def tab_changed(self):
        tab_position = self.tabWidget.currentIndex()
        LOGGER.info(f'Tab changed {tab_position}')
        self.send_event(TabChangedEvent(tab_position))

    def open_mri_dir(self):
        # TODO: filter files by extension
        path_to_dir = str(QFileDialog.getExistingDirectory(self,
                                                           "Select Directory",
                                                           '',
                                                           QFileDialog.DontUseNativeDialog))
        if path_to_dir:
            if os.sep == '\\':
                path_to_dir = path_to_dir.replace('/', '\\')
            self._open_mri_dir(path_to_dir)
        else:
            LOGGER.info("No directory chosen")

    def _open_mri_dir(self, path_to_dir):
        try:
            original_dcm = load_mri(path_to_dir)
        except InvalidDicomError:
            LOGGER.error('Incorrect directory content.')
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.showMessage(f'Directory by path {path_to_dir} didn\'t contain any medical images')
            error_dialog.exec_()
            return
        self.original_data = np.array([array.pixel_array for array in original_dcm])
        normed_data = np.array([rescale_original_image(array) for array in self.original_data])

        self.send_event(OriginalDataLoaded(normed_data))

    def open_npz_file(self):
        path_to_file = QFileDialog.getOpenFileName(self, "Select npz file")[0]
        if path_to_file:
            self.close_research()
            loaded = np.load(path_to_file, allow_pickle=True)
            original_data = loaded.get('original_data')
            stage_params = loaded.get('stage_params').item()
            self.send_event(OriginalDataLoaded(original_data))
            for stage_name, sep_stage_params in stage_params.items():
                LOGGER.info(f"{stage_name}, {sep_stage_params}")

    def save_npz_file(self):
        # should generate filename from mri info
        tmp_file_name = f'tst_research_{datetime.now().timestamp()}'
        path_to_save = str(QFileDialog.getSaveFileName(self, "Select file to save", f"{tmp_file_name}.npz")[0])
        result_stage_params = {}
        for i in range(self.tabWidget.count()):
            cur_tab = self.tabWidget.widget(i)
            tab_name = cur_tab.stage_name
            stage_params = cur_tab.get_stage_params()
            result_stage_params[tab_name] = stage_params
        np.savez(path_to_save, original_data=self.original_data, stage_params=result_stage_params)

    def close_research(self):
        self.send_event(ClearAllEvent())
