from PyQt5 import QtCore, QtWidgets

from spine_detector.constants import LOGGER
from spine_detector.event_logic import *
from spine_detector.widgets.segmentation.common_segmentation_widget import SegmentationCommonWidget


class UI_SegmentationTabWidget(object):

    def setupUi(self, segmentation_tab_widget):
        segmentation_tab_widget.setObjectName("filtered_tab_widget")
        segmentation_tab_widget.resize(1200, 800)
        segmentation_tab_widget.setMinimumSize(QtCore.QSize(1200, 800))
        segmentation_tab_widget.setMaximumSize(QtCore.QSize(1200, 800))

        self.central_layout = QtWidgets.QVBoxLayout(segmentation_tab_widget)
        self.btns_vertical_layout = QtWidgets.QHBoxLayout(segmentation_tab_widget)
        self.segmentation_enable_rbtn = QtWidgets.QRadioButton(segmentation_tab_widget)
        self.segmentation_enable_rbtn.setText('Segmentation enabled')
        self.segmentation_enable_rbtn.setAutoExclusive(False)
        self.segmentation_enable_rbtn.setEnabled(False)
        self.btns_vertical_layout.addWidget(self.segmentation_enable_rbtn)

        spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.btns_vertical_layout.addItem(spacer)
        self.central_layout.addLayout(self.btns_vertical_layout)

        self.common_segmentation_widget: SegmentationCommonWidget = SegmentationCommonWidget(segmentation_tab_widget)
        self.common_segmentation_widget.setEnabled(False)
        self.central_layout.addWidget(self.common_segmentation_widget)

        # self.vertical_layout = QtWidgets.QHBoxLayout(segmentation_tab_widget)
        # self.central_layout.addLayout(self.vertical_layout)


class SegmentationTabWidgetV2(QtWidgets.QWidget, UI_SegmentationTabWidget, Component):
    common_segmentation_widget: SegmentationCommonWidget

    def __init__(self, parent=None, **kwargs):
        super().__init__(parent=parent)
        self.setupUi(self)
        self.stage_name = kwargs.get('stage_name', 'segmentation')
        # internal state
        self.current_position = 0
        self.segmentation_enabled = False
        self.colored_enabled = False
        # internal state
        self.segmentation_enable_rbtn.clicked.connect(self.segmentation_enabled_click)
        # event
        self.event_handler = {
            OriginalDataLoaded: self.original_data_loaded_action,
            FilterMaskUpdated: self.filter_mask_updated_action,
            SliceUpdateEvent: self.slice_update_action,
            ClearAllEvent: self.clear_all_action,
        }

    # region handle clicks
    def segmentation_enabled_click(self):
        if self.segmentation_enable_rbtn.isChecked():
            self.common_segmentation_widget.setEnabled(True)
            self.segmentation_enabled = True
            self.refresh()
            self.notify()
        else:
            self.common_segmentation_widget.setEnabled(False)
            # clean views
            self.segmentation_enabled = False

    # endregion

    # region handle event
    def original_data_loaded_action(self, event: OriginalDataLoaded):
        LOGGER.info('original_data_loaded_action')
        self.set_original_data(original_data=event.original_data)

    def filter_mask_updated_action(self, event: FilterMaskUpdated):
        self.segmentation_enable_rbtn.setEnabled(True)
        LOGGER.info('filter_mask_update action')
        self.set_source_data(event.filtered_data)
        self.refresh()

    def slice_update_action(self, event: SliceUpdateEvent):
        LOGGER.info('slice_update_action')
        self.current_position = event.position
        self.slice_update()

    def clear_all_action(self, event: ClearAllEvent):
        LOGGER.info('clear_all_action')
        self.clear_all()

    # endregion handle event
    def notify(self):
        # called from child
        regions_3d = self.common_segmentation_widget.get_regions_3d()
        self.send_event(SegmentationUpdated(regions_3d))

    def set_original_data(self, original_data):
        self.common_segmentation_widget.set_original_data(original_data)

    def set_source_data(self, source_data):
        self.common_segmentation_widget.set_source_data(source_data)
        if self.segmentation_enabled:
            self.common_segmentation_widget.refresh(self.current_position, self.colored_enabled)

    def slice_update(self):
        self.common_segmentation_widget.current_position = self.current_position
        if self.segmentation_enabled:
            self.common_segmentation_widget.update_slice(self.current_position)

    def refresh(self):
        if self.segmentation_enabled:
            self.common_segmentation_widget.refresh(self.current_position, self.colored_enabled)

    def clear_all(self):
        self.segmentation_enable_rbtn.setEnabled(False)
        self.common_segmentation_widget.clean_all()
        self.common_segmentation_widget.setEnabled(False)
        self.current_position = 0

    def get_stage_params(self) -> Dict:
        filter_params = dict()
        return {f'{self.stage_name}': dict()}
