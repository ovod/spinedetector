from PyQt5.QtWidgets import QApplication

from spine_detector.constants import LOGGER

from PyQt5 import QtWidgets
import numpy as np

from spine_detector.core_logic.filtration import FILTER_MAP
from spine_detector.domain_classes import SliderType, FilterTypes
from spine_detector.widgets.common.custom_image_view import CustomImageView
from spine_detector.widgets.common.parameters_box_widget import ParameterSliderBoxWidget


class SingleFilterWidget(QtWidgets.QWidget):

    def setup_ui(self, options_specification):
        self.setFixedSize(400, 700)

        vertical_layout = QtWidgets.QVBoxLayout(self)
        self.filter_label = QtWidgets.QLabel()
        vertical_layout.addWidget(self.filter_label)

        self.control_box = QtWidgets.QGroupBox(self)
        control_box_layout = QtWidgets.QHBoxLayout()
        self.control_box.setLayout(control_box_layout)

        self.is_used_rbtn = QtWidgets.QRadioButton(self)
        self.is_used_rbtn.setText('Is used')
        self.is_used_rbtn.setChecked(True)
        self.is_used_rbtn.setAutoExclusive(False)

        control_box_layout.addWidget(self.is_used_rbtn)

        self.advanced_rbt = QtWidgets.QRadioButton(self)
        self.advanced_rbt.setText('Advanced options')
        self.advanced_rbt.setChecked(False)
        self.advanced_rbt.setAutoExclusive(False)

        control_box_layout.addWidget(self.advanced_rbt)

        self.reset_btn = QtWidgets.QPushButton(self)
        self.reset_btn.setText('Reset to defaults')

        control_box_layout.addWidget(self.reset_btn)
        self.control_box.setEnabled(False)

        vertical_layout.addWidget(self.control_box)

        self.controlled_image = CustomImageView(self)
        vertical_layout.addWidget(self.controlled_image)

        self.filter_parameters_widget = ParameterSliderBoxWidget(self, options_specification)
        self.filter_parameters_widget.set_sliders_enabled(False)
        vertical_layout.addWidget(self.filter_parameters_widget)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        vertical_layout.addItem(spacerItem)

    def __init__(self, core, parent=None):
        super(SingleFilterWidget, self).__init__(parent=parent)
        self.parent_widget = parent
        self.setup_ui(core.get_specification())
        self.core = core
        self.filter_label.setText(core.name)
        self.is_used_state = True
        self.advanced_opt_active = False
        self.is_used_rbtn.clicked.connect(self.is_used_state_changed)
        self.advanced_rbt.clicked.connect(self.advanced_parameters_set_activate)

        self.filter_parameters_widget.set_parameter_handler(self.default_parameter_handler)
        self.current_position = 0
        self.control_widget = None

    def is_used_state_changed(self):
        if self.is_used_rbtn.isChecked():
            self.is_used_state = True
            self.default_parameter_handler()
        else:
            self.is_used_state = False
            self.default_parameter_handler()

    def advanced_parameters_set_activate(self):
        if self.advanced_rbt.isChecked():
            self.advanced_opt_active = True
            self.set_options_state(True, SliderType.ADVANCED)
        else:
            self.advanced_opt_active = False
            self.set_options_state(False, SliderType.ADVANCED)

    def post_init(self, core_instance):
        self.core = core_instance

    def set_controller(self, control_widget):
        self.control_widget = control_widget

    def default_parameter_handler(self):
        if self.is_used_state:
            current_values = self.filter_parameters_widget.get_current_values()
            LOGGER.debug(f'Handled: {current_values}')
            self.update_filtration()
            self.refresh()

        if self.control_widget:
            self.control_widget._sep_filter_params_updated()

    def set_original_data(self, original_data: np.ndarray):
        self.core.set_original_data(original_data)

    def set_options_state(self, state: bool, sliders_type: SliderType = None):
        self.filter_parameters_widget.set_sliders_enabled(state, sliders_type)

    def update_filtration(self):
        if self.is_used_state:
            new_filtration_params = self.filter_parameters_widget.get_current_values()
            self.core.update_filtration(new_filtration_params)

    def refresh(self, new_position=None):
        if not new_position:
            new_position = self.current_position
        else:
            self.current_position = new_position

        self.controlled_image.set_image(self.core.get_filtered_image(new_position))

    def get_filter_mask(self):
        if self.is_used_state:
            return self.core.get_filtered_mask()
        else:
            return 1

    def clear_data(self):
        self.core.clean_data()
        self.current_position = 0
        self.controlled_image.set_image(np.zeros((320, 320)))

    def update_params(self, new_params):
        self.filter_parameters_widget.update_params(new_params)

    def get_filter_params(self):
        return self.filter_parameters_widget.get_current_values()


def generate_filter_widget(filter_type: FilterTypes) -> SingleFilterWidget:
    core_type = FILTER_MAP.get(filter_type)
    return SingleFilterWidget(core_type())


# app = QApplication([])
# widget = generate_filter_widget(FilterTypes.EDGE_FILTER)
# widget.setWindowTitle('SingleFilterWidget')
# widget.show()
# app.exec_()