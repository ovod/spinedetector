import numpy as np

from spine_detector.masking.base import filter_by_mask
from spine_detector.masking.gradient import get_gradient_mask, get_gradient
from spine_detector.filters.otsu import otsu_with_borders
from spine_detector.utils import load_mri_research, prepare_for_view

filtered_data = np.zeros((15, 320, 320))
base_result_path = '../results'

first_patient_path = '../test-data/0001'
T1_research = f'{first_patient_path}/T1_TSE_SAG_320_0003'

pixel_arrays, _ = load_mri_research(T1_research)

print(f'Stack data')
original_data = np.zeros((15, 320, 320))

for i, array in enumerate(pixel_arrays):
    original_data[i] = array


k_min = 15
k_max = 255
print(f'Step {k_min} {k_max}')

gradient_mask = get_gradient_mask(original_data)

for l, array in enumerate(pixel_arrays):
    filtered_data[l] = otsu_with_borders(array)

filtered_data = filter_by_mask(filtered_data, gradient_mask, k_min=k_min, k_max=k_max)

result = np.zeros((100, 320, 320, 4))

for l in range(len(original_data)):
    result += prepare_for_view(filtered_data[l], position=l)

np.savez_compressed(f'{base_result_path}/gradient_{k_min}_{k_max}', data=result)

