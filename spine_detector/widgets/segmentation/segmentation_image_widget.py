from skimage.color import gray2rgb

from spine_detector.constants import LOGGER

import numpy as np

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QPointF

from spine_detector.core_logic import SegmentationCore
from spine_detector.widgets.common.custom_image_view import CustomImageView
from spine_detector.widgets.common.parameters_box_widget import ParameterSliderBoxWidget


class SegmentationImageWidget(QtWidgets.QWidget):
    def setup_ui(self, options_spec):
        self.setFixedSize(QtCore.QSize(400, 800))

        central_layout = QtWidgets.QVBoxLayout(self)

        self.segmentation_2d_label = QtWidgets.QLabel(self)
        self.segmentation_2d_label.setText('View in 2D')
        central_layout.addWidget(self.segmentation_2d_label)

        self.controlled_image = CustomImageView(self)
        central_layout.addWidget(self.controlled_image)

        self.segmentation_params = ParameterSliderBoxWidget(self, options_spec)
        central_layout.addWidget(self.segmentation_params)

        self.segmentation_clicked_info_label = QtWidgets.QLabel()
        self.segmentation_clicked_info_label.setFixedSize(380, 150)

        central_layout.addWidget(self.segmentation_clicked_info_label)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        central_layout.addItem(spacerItem)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.core = SegmentationCore()
        self.setup_ui(self.core.get_specification())

        self.segmentation_params.set_parameter_handler(self.segmentation_parameters_changed)
        self.current_position = 0
        self.controlled_image.custom_behavior = True
        self.controlled_image.set_behavior_handler(self.custom_image_behavior)

    def set_source_data(self, source_data):
        self.core.set_source_data(source_data)

    def clean_all(self):
        self.core.clean_all()
        self.current_position = 0

    def update_segmentation(self):
        new_params = self.segmentation_params.get_current_values()
        LOGGER.info(f'Segmentation params changed to {new_params}')
        self.core.update_segmentation(new_params)

    def get_binary_segmentation(self):
        return self.core.get_binary_segmentation_data()

    def get_labeled_3d_data(self):
        return self.core.labeled_data_3d

    def get_components_3d(self):
        return self.core.get_regions_3d()

    def refresh(self, position, colored_enabled=False):
        if position is not None:
            self.current_position = position
        else:
            position = self.current_position

        source_data = self.core.source_data[position]
        survived_regions = self.core.get_binary_segmentation_image(position)
        result_image = gray2rgb(source_data.copy())
        # colorize survived image as green
        r, g, b = result_image[:, :, 0], result_image[:, :, 1], result_image[:, :, 2]
        r[survived_regions == 1] = 0
        g[survived_regions == 1] = 1
        b[survived_regions == 1] = 0

        self.controlled_image.set_image(result_image)

    def segmentation_parameters_changed(self):
        LOGGER.debug(f'Default handler {self.segmentation_params.get_current_values()}')
        self.update_segmentation()
        self.refresh(self.current_position, False)

    def custom_image_behavior(self, point: QPointF):
        x_pos, y_pos = int(point.x()), int(point.y())
        LOGGER.debug(f'Clicked on position {x_pos} {y_pos}')
        current_label_image = self.core.get_label_image_by_position(self.current_position)
        picked_label = current_label_image[y_pos, x_pos]
        region = next(x for x in self.core.region_props[self.current_position] if x.label == picked_label)
        area = region.area
        ratio = get_aspect_ration(region)
        another_ratio = region.perimeter / np.sqrt(region.area)

        self.segmentation_clicked_info_label.setText(
            f'Picked label: {picked_label}. Area: {area} px. Orientation: {region.orientation} \n'
            f'Ratio {round(ratio, 2)}. P/A ratio: {round(another_ratio, 2)}')


def get_aspect_ration(region):
    min_y, min_x, max_y, max_x = region.bbox
    y_len = max_y - min_y
    x_len = max_x - min_x
    return x_len / y_len
