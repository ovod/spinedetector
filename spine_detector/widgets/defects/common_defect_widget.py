from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
import numpy as np
from scipy.ndimage import binary_dilation, binary_erosion
from skimage.color import gray2rgb
from skimage.measure._regionprops import RegionProperties

from spine_detector.constants import LOGGER
from spine_detector.masking.gradient import get_gradient
from spine_detector.utils import timer
from spine_detector.widgets.defects.component_list import DefectComponentList
from spine_detector.widgets.defects.view_3d import DefectView3d


@timer
def curvature_defect_search(region: RegionProperties, radius=15, threshold_min=0.55, threshold_up=0.8):
    filled_image = region.filled_image
    defect_map = np.zeros_like(filled_image, dtype=np.float32)
    for i in range(filled_image.shape[0]):
        current_slice = filled_image[i]
        current_slice = np.pad(current_slice, (radius, radius))
        slice_defect_image = np.zeros_like(current_slice, dtype=float)
        # get edge
        edge = current_slice ^ binary_erosion(current_slice)
        slice_coords = tuple(zip(*np.where(edge != 0)))
        for coord in slice_coords:
            x, y = coord
            roi = current_slice[x - radius:x + radius + 1, y - radius:y + radius + 1]
            filled_value = np.sum(roi) / ((2 * radius) ** 2)
            slice_defect_image[x, y] = filled_value
        defect_map[i] = slice_defect_image[radius:-radius, radius:-radius]
    return ((defect_map > threshold_min) * (defect_map < threshold_up)) * (defect_map > 0.01)


def generate_data_with_defects(original_data, defects_data, binary_mask, gradient_data):
    gradient_defect = gradient_data > 40
    original_data /= 255.
    original_data *= binary_mask
    result_data = gray2rgb(original_data.copy(), alpha=True)
    for i in range(original_data.shape[0]):
        curr_defects = defects_data[i]
        r, g, b, a = result_data[i, :, :, 0], result_data[i, :, :, 1], result_data[i, :, :, 2], result_data[i, :, :, 3]
        r[gradient_defect[i]] = 0.79
        g[gradient_defect[i]] = 0.45
        b[gradient_defect[i]] = 0.12

        r[curr_defects] = 0.78
        g[curr_defects] = 0.29
        b[curr_defects] = 0.12

        a[binary_mask[i] == False] = 0
    return result_data * 255

class DefectSearcherWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(DefectSearcherWidget, self).__init__(parent=parent)
        self.setFixedSize(1200, 800)

        central_layout = QtWidgets.QHBoxLayout(self)

        self.components_list = DefectComponentList(self)
        central_layout.addWidget(self.components_list, 0, Qt.AlignTop)

        self.view_3d = DefectView3d(self)
        central_layout.addWidget(self.view_3d, 0, Qt.AlignTop)

        central_layout.addItem(
            QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))

        self.post_init()

    def post_init(self):
        self.components_list.item_select_action = self.component_selected_action_v2
        self.components_list.item_unselect_action = self.component_unselected_action

    def set_original_data(self, original_data):
        self.original_data = original_data

    def set_components_3d(self, components_3d):
        self.components_3d = components_3d

    def refresh(self):
        LOGGER.debug(f'Added new components')
        self.components_list.clear_all()
        self.components_list.add_items(self.components_3d)

    def component_selected_action(self, item):
        region = next(x for x in self.components_3d if str(x.label) == item.text())
        self.view_3d.clear_all()
        self.view_3d.set_data(region.filled_image.astype(np.uint8) * 255)

    def component_selected_action_v2(self, item):
        region = next(x for x in self.components_3d if str(x.label) == item.text())
        original_image_region = self.original_data[region.slice]
        binary_image_region = region.filled_image
        gradient_image_region = get_gradient(original_image_region, alpha=0.2)
        defect_data = generate_data_with_defects(original_image_region.copy(),
                                                 curvature_defect_search(region),
                                                 binary_image_region,
                                                 gradient_image_region)
        if self.parent():
            self.parent().region_picked(defect_data, original_image_region)
        self.view_3d.update_data_v2(original_image_region, binary_image_region, gradient_image_region, defect_data)

    def component_unselected_action(self, item):
        self.view_3d.clear_all()

    def clear_all(self):
        self.original_data = None
        self.components_3d = None
