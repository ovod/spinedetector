from unittest import TestCase

from spine_detector.old_core import SpineDetectorCore

TEST_DATA = "test-data/0001/T1_TSE_SAG_320_0003"


def core_is_empty(core_instance):
    return not any(core_instance.__dict__.values())


class TestSpineDetectorCore(TestCase):

    def setUp(self) -> None:
        self.core = SpineDetectorCore()

    def test_on_clean_while_data_not_loaded(self):
        # when
        self.core.clean_data()
        # then
        self.assertTrue(core_is_empty(self.core))

    def test_on_clean_on_data_loaded(self):
        # given
        self.core.read_mri_dir(TEST_DATA)
        # when
        self.core.clean_data()
        # then
        self.assertTrue(core_is_empty(self.core))

    def test_on_clean_on_data_loaded_and_processed(self):
        # given
        self.core.read_mri_dir(TEST_DATA)
        # and
        self.core.update_filtration()
        # when
        self.core.clean_data()
        # then
        self.assertTrue(core_is_empty(self.core))

    def tearDown(self) -> None:
        del self.core
