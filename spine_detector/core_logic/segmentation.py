from typing import Dict, List

from scipy.ndimage import binary_erosion, binary_dilation

from spine_detector.constants import LOGGER
from skimage.measure import label, regionprops
from skimage.measure._regionprops import RegionProperties
import numpy as np

from spine_detector.utils import timer


def filter_aspect_ratio(region, min_value, max_value):
    min_y, min_x, max_y, max_x = region.bbox
    y_len = max_y - min_y
    x_len = max_x - min_x
    return min_value < x_len / y_len < max_value


class SegmentationCore:
    def __init__(self):
        self.segmentation_params: Dict = {}

        self.source_data: np.ndarray = None
        self.binary_segmentation_data: np.ndarray = None
        self.labeled_data: np.ndarray = None

    def clean_all(self):
        self.source_data = None
        self.binary_segmentation_data = None

    def get_binary_segmentation_data(self):
        return self.binary_segmentation_data

    def get_binary_segmentation_image(self, position):
        return self.binary_segmentation_data[position]

    def get_label_image_by_position(self, position):
        return self.labeled_data[position]

    def set_source_data(self, source_data):
        self.source_data = source_data
        self.binary_segmentation_data = np.zeros(source_data.shape, dtype=np.uint16)
        self.labeled_data = np.zeros(source_data.shape, dtype=np.uint16)
        self.labeled_data_3d = np.zeros(source_data.shape, dtype=np.uint16)
        self.region_props = [None] * source_data.shape[0]
        self.region_3d: List[RegionProperties] = []

    def get_region_props_on_position(self, position):
        return self.region_props[position]

    def get_all_region_props(self):
        return self.region_props

    def get_regions_3d(self):
        return self.region_3d

    @timer
    def _update_segmentation_2d(self):
        pass

    @timer
    def update_segmentation(self, params):
        self.segmentation_params.update(params)
        for i in range(0, self.source_data.shape[0]):
#            object_mask = np.zeros_like(self.source_data[i], dtype=np.bool)
#            object_mask = binary_erosion(self.source_data[i], iterations=self.segmentation_params['erosion_iter'])
#           object_mask = binary_dilation(object_mask, iterations=self.segmentation_params['dilation_iter'])

            self.labeled_data[i], num = label(self.source_data[i], return_num=True)
            LOGGER.debug(f'On {i} image found {num} label(s).')

            object_mask = np.zeros_like(self.labeled_data[i], dtype=np.bool)
            regions_2d: List = regionprops(self.labeled_data[i])
            # filter by perimeter
            regions_2d = list(filter(
                lambda x: self.segmentation_params['p_min_size']
                          < x.perimeter / np.sqrt(x.filled_area) <
                          self.segmentation_params['p_max_size'],
                regions_2d))
            # filter by area
            regions_2d = list(filter(
                lambda x: self.segmentation_params['s_min_size']
                          < x.filled_area <
                          self.segmentation_params['s_max_size'],
                regions_2d))
            # search for square areas
            regions_2d = list(filter(lambda x: filter_aspect_ratio(x, 0.5, 1.5), regions_2d))

            LOGGER.info(f'Region perimeter {[x.perimeter for x in regions_2d]}')
            LOGGER.info(f'Region area {[x.filled_area for x in regions_2d]}')

            for region in regions_2d:
                object_mask[region.slice] += region.filled_image

            self.region_props[i] = np.array(regions_2d)
            self.binary_segmentation_data[i] = object_mask
        LOGGER.info(f'Segmentation with params {self.segmentation_params} completed.')
        LOGGER.info(f'Merge into 3d model')
        self.labeled_data_3d, num = label(self.binary_segmentation_data, return_num=True)
        LOGGER.info(f'Found {num} region(s).')
        self.region_3d = regionprops(self.labeled_data_3d)
        self.region_3d = list(filter(
            lambda x: x.slice[0].stop - x.slice[0].start > 2, self.region_3d
        ))
        self.region_3d = self._sort_3d_regions(self.region_3d)
        self.binary_segmentation_data = np.zeros_like(self.binary_segmentation_data)
        for region_3d in self.region_3d:
            self.binary_segmentation_data[region_3d.slice] += region_3d.filled_image

    @staticmethod
    def _sort_3d_regions(regions_3d: List[RegionProperties]):
        if not regions_3d:
            return []

        sorted_regions_3d = list(sorted(regions_3d, key=lambda x: x.centroid[1]))
        for i, region in enumerate(sorted_regions_3d):
            region.label = i + 1
        return sorted_regions_3d


    @staticmethod
    def merge_regions(region_1, region_2) -> RegionProperties:
        label = -1
        new_start_0 = min(region_1.slice[0].start, region_2.slice[0].start)
        new_stop_0 = max(region_1.slice[0].stop, region_2.slice[0].start)
        new_start_1 = min(region_1.slice[1].start, region_2.slice[1].start)
        new_stop_1 = max(region_1.slice[1].stop, region_2.slice[1].start)
        new_start_2 = min(region_1.slice[2].start, region_2.slice[2].start)
        new_stop_2 = max(region_1.slice[2].stop, region_2.slice[2].start)

        new_slice = (
            slice(new_start_0, new_stop_0),
            slice( new_start_1, new_stop_1,),
            slice(new_start_2, new_stop_2)
        )
        return RegionProperties(new_slice, label, region_1._label_image)




    @staticmethod
    def get_specification():
        return dict(
            p_min_size=dict(label='Ratio min', value_type='float', init_value=3.5, min_value=3., max_value=4.5),
            p_max_size=dict(label='Ratio max', value_type='float', init_value=5.2, min_value=4.5, max_value=7.),
            s_min_size=dict(label='Area min', init_value=900, min_value=700, max_value=1800),
            s_max_size=dict(label='Area max', init_value=1800, min_value=600, max_value=2400),
            erosion_iter=dict(label='Erosion iter', min_value=1, max_value=30),
            dilation_iter=dict(label='Dilation iter', min_value=1, max_value=30, init_value=3),
        )
