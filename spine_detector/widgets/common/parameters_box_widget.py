from typing import Dict, Callable, List
from spine_detector.constants import LOGGER

from PyQt5 import QtCore, QtWidgets

from spine_detector.domain_classes import SliderType
from spine_detector.widgets.common.parameter_slider_widget import ParameterSliderWidget, LogParameterSlider, \
    FloatParameterSlider


class ParameterSliderBoxWidget(QtWidgets.QWidget):

    def setup_ui(self, sliders_definitions):
        central_layout = QtWidgets.QVBoxLayout(self)
        self.general_slider_map: Dict[str, ParameterSliderWidget] = {}
        self.advanced_slider_map: Dict[str, ParameterSliderWidget] = {}
        height = len(sliders_definitions) * 25 + 10
        self.resize(380, height)
        self.setFixedSize(QtCore.QSize(370, height))

        for name, info in sliders_definitions.items():
            slider_type = info.get('value_type')
            if slider_type is 'float':
                slider = FloatParameterSlider(self, name=name, **info)
            elif slider_type is 'log':
                slider = LogParameterSlider(self, name=name, **info)
            else:
                slider = ParameterSliderWidget(self, name=name, **info)
            if info.get('slider_type', SliderType.GENERAL) is SliderType.GENERAL:
                self.general_slider_map[name] = slider
            else:
                self.advanced_slider_map[name] = slider

            central_layout.addWidget(slider, 0, QtCore.Qt.AlignTop)

        central_layout.addItem(
            QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        )


    def __init__(self, parent, slider_definitions, box_name='Default box'):
        super().__init__(parent=parent)
        self.setup_ui(slider_definitions)
        self.box_name = box_name

        self.set_parameter_handler()

    def set_parameter_handler(self, new_func: Callable = None):
        if not new_func:
            new_func = self.parameters_on_change
        for slider in {**self.general_slider_map, **self.advanced_slider_map}.values():
            slider.parameter_slider.sliderReleased.disconnect()
            slider.parameter_slider.sliderReleased.connect(new_func)

    def parameters_on_change(self):
        new_params = self.get_current_values()
        LOGGER.info(f'{self.box_name}: Parameters changed to {new_params}')

    def get_current_values(self):
        current_params = dict()
        for name, slider in {**self.general_slider_map, **self.advanced_slider_map}.items():
            value = round(slider.value(), 2)
            current_params[slider.slider_name] = value
        return current_params

    def update_params(self, new_params_values: Dict):
        for slider_name, new_value in new_params_values.items():
            if slider_name in self.general_slider_map.keys():
                self.general_slider_map[slider_name].set_value(new_value)

    def set_sliders_enabled(self, state: bool, slider_type: SliderType = SliderType.GENERAL):
        if slider_type is SliderType.GENERAL:
            if state:
                for slider in self.general_slider_map.values():
                    slider.setEnabled(state)
                for slider in self.advanced_slider_map.values():
                    slider.setEnabled(state)
            else:
                for slider in self.general_slider_map.values():
                    slider.setEnabled(state)
                for slider in self.advanced_slider_map.values():
                    slider.setEnabled(state)
                    slider.hide()
        else:
            if state:
                # show advanced
                for slider in self.advanced_slider_map.values():
                    slider.show()
            else:
                # hide advanced
                for slider in self.advanced_slider_map.values():
                    slider.hide()

        # if not state and not slider_type:
        #     self.set_box_enabled(True)
        # if slider_type:
        #     need_to_change = list(filter(lambda x: x.slider_type == slider_type, self.general_slider_map.values()))
        # else:
        #     need_to_change = self.general_slider_map.values()
        # for slider in need_to_change:
        #     slider.setEnabled(state)
