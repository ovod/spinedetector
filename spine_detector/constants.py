import logging
import sys

LOGGER = logging.getLogger('spine_detector')
LOGGING_FORMAT = '[%(levelname)s] %(asctime)s %(filename)s:%(funcName)s:%(lineno)s: %(message)s'


def add_console_handler():
    formatter = logging.Formatter(LOGGING_FORMAT)
    LOGGER.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(formatter)
    LOGGER.addHandler(console_handler)


def set_log_level(level, logger_name='spine_detector'):
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)


filtration_big_box_params = {
    'infl_k': 0.9,
    'grad_degree': 0.9,
    'alpha': 0.2,
    'x': 1.16,
    'y': 1.82,
    'z': 2.73,
    'k_min': 21.,
    'light': 1
}

filtration_small_box_params = {
    'infl_k': 0.9,
    'grad_degree': 1.8,
    'alpha': 0.2,
    'x': 0.1,
    'y': 0.09,
    'z': 0.09,
    'k_min': 12.8,
}

edge_filtration_default_params = dict(
    alpha=0.2,
    gamma=0.75,
    k_min=45.
)

threshold_filter_params = {
    'threshold': 45
}

vert_segmentation_params = {
    'vert_min_size': 800,
    'vert_max_size': 1200,
}

disk_segmentation_params = {
    'disk_min_size': 200,
    'disk_max_size': 400,
}
