from PyQt5.QtWidgets import *
import sys


class GroupBox(QWidget):

    def __init__(self):
        QWidget.__init__(self)

        self.setWindowTitle("GroupBox")
        layout = QGridLayout()
        self.setLayout(layout)

        self.groupbox = QGroupBox("GroupBox Example")
        # groupbox.setCheckable(True)
        layout.addWidget(self.groupbox)

        vbox = QHBoxLayout()
        self.groupbox.setLayout(vbox)

        radiobutton1 = QRadioButton("RadioButton 1 \n  + \n RADADASDASDA")
        radiobutton1.__setattr__('value', 1)
        radiobutton1.clicked.connect(self.click)
        vbox.addWidget(radiobutton1)

        radiobutton2 = QRadioButton("RadioButton 2")
        radiobutton2.__setattr__('value', 2)
        radiobutton2.clicked.connect(self.click)
        vbox.addWidget(radiobutton2)

        radiobutton3 = QRadioButton("RadioButton 3")
        radiobutton3.__setattr__('value', 3)
        radiobutton3.clicked.connect(self.click)
        vbox.addWidget(radiobutton3)

        radiobutton4 = QRadioButton("RadioButton 4")
        radiobutton4.__setattr__('value', 4)
        radiobutton4.clicked.connect(self.click)
        vbox.addWidget(radiobutton4)
        self.btns = [radiobutton1, radiobutton2, radiobutton3, radiobutton4]

    def click(self):
        clicked_on = next(x for x in self.btns if x.isChecked())
        print(clicked_on)
        print(clicked_on.text())
        print(clicked_on.value)


app = QApplication(sys.argv)
screen = GroupBox()
screen.show()
sys.exit(app.exec_())
