from spine_detector.constants import LOGGER

import numpy as np
from scipy.ndimage import gaussian_filter, gaussian_filter1d

from spine_detector.utils import timer

# TODO: тесты
@timer
def convole(norm, size=(3, 3, 3)):
    csum = np.cumsum(norm, axis=0)
    csum = np.cumsum(csum, axis=1)
    csum = np.cumsum(csum, axis=2)

    csum = csum[2 * size[0]:] - csum[:-2 * size[0]]
    csum = csum[:, 2 * size[1]:] - csum[:, :-2 * size[1]]
    csum = csum[:, :, 2 * size[2]:] - csum[:, :, :-2 * size[2]]

    result = np.zeros_like(norm)
    result[size[0]: -size[0], size[1]: -size[1], size[2]: -size[2]] = csum

    return result

@timer
def get_gradient(data, alpha) -> np.ndarray:
    gradient = [np.diff(data, 1, axis=0) * alpha,
                np.diff(data, 1, axis=1),
                np.diff(data, 1, axis=2)]
    norm_x = np.zeros_like(data)
    norm_y = np.zeros_like(data)
    norm_z = np.zeros_like(data)

    norm_x[:-1] += gradient[0]
    norm_x[1:] += gradient[0]

    norm_y[:, :-1] += gradient[1]
    norm_y[:, 1:] += gradient[1]

    norm_z[:, :, :-1] += gradient[2]
    norm_z[:, :, 1:] += gradient[2]

    norm = np.sqrt(np.power(norm_x, 2) + np.power(norm_y, 2) + np.power(norm_z, 2))

    assert not np.argwhere(np.isnan(norm))
    assert np.all(norm >= 0)
    return norm


@timer
def box_filtering_with_gauss(data, degree=2, size=(3, 3, 3), alpha=0, sigma=2):
    gradient = get_gradient(data, alpha=alpha) ** degree
    filtered = gaussian_filter(gradient, sigma)

    csum = np.cumsum(filtered, axis=0)
    csum = np.cumsum(csum, axis=1)
    csum = np.cumsum(csum, axis=2)

    csum = (csum[2 * size[0]:] - csum[:-2 * size[0]]) / (2 * size[0] + 1)

    csum = (csum[:, 2 * size[1]:] - csum[:, :-2 * size[1]]) / (2 * size[1] + 1)

    csum = (csum[:, :, 2 * size[2]:] - csum[:, :, :-2 * size[2]]) / (2 * size[2] + 1)

    csum = np.power(csum, 1 / degree)

    result = np.zeros_like(data)
    result[size[0]: -size[0], size[1]: -size[1], size[2]: -size[2]] = csum

    result = np.nan_to_num(result)
    #assert not np.argwhere(np.isnan(result))

    return result

@timer
def box_filtering_via_gauss(data, degree=2, size=(3, 3, 3)):
    LOGGER.info(f'Degree {degree}')
    gradient = data ** degree

    filtered = gaussian_filter1d(gradient, size[0], axis=0, truncate=2., mode='constant')
    filtered = gaussian_filter1d(filtered, size[1], axis=1, truncate=2., mode='constant')
    filtered = gaussian_filter1d(filtered, size[2], axis=2, truncate=2., mode='constant')

    filtered = np.power(filtered, 1 / degree)

    return filtered
